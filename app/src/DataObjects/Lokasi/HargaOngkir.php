<?php

use SilverStripe\Forms\TextField;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class HargaOngkir extends DataObject
{
    /**
     * Singular name for CMS
     *  @var string
     */
    private static $singular_name = 'Harga ongkir';

    /**
     * Plural name for CMS
     *  @var string
     */
    private static $plural_name = 'Harga ongkir';

    private static $db = [
        'Harga' => 'Int',
        'Size' => 'Int',
        'Type' => 'Int'
    ];

    private static $has_one = [
        'DariKota' => Kota::class,
        'DariKotaBagian' => KotaBagian::class,
        'KeKota' => Kota::class,
        'KeKotaBagian' => KotaBagian::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'From' => 'Dari',
        'To' => 'Ke',
        'Harga' => 'Harga',
        'SizeToString' => 'Ukuran',
        'TypeToString' => 'Type'
    ];

    /**
     * Defines a default list of filters for the search context
     * @var array
     */
    private static $searchable_fields = [
        'DariKota.Title',
        'KeKota.Title'
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            LiteralField::create('JudulFrom', '<h1>Dari</h1>'),
            DropdownField::create(
                'DariKotaID',
                'Kota',
                Kota::get()->map('ID', 'Title')
            ),
            DropdownField::create(
                'DariKotaBagianID',
                'Bagian',
                KotaBagian::get()->map('ID', 'Title')
            ),
            LiteralField::create('JudulTo', '<h1>Ke</h1>'),
            DropdownField::create(
                'KeKotaID',
                'Kota',
                Kota::get()->map('ID', 'Title')
            ),
            DropdownField::create(
                'KeKotaBagianID',
                'Bagian',
                KotaBagian::get()->map('ID', 'Title')
            ),
            LiteralField::create('JudulTo', '<h1>Harga</h1>'),
            TextField::create(
                'Harga',
                'Harga'
            ),
            DropdownField::create(
                'Size',
                'Size',
                [
                    0 => 'All size',
                    1 => 'Small',
                    2 => 'Medium',
                    3 => 'Large'
                ]
            ),
            DropdownField::create(
                'Type',
                'Type',
                [
                    0 => 'Express',
                    1 => 'Sameday'
                ]
            )
        );
    }

    public function From()
    {
        return $this->DariKota->Title . ' ' . $this->DariKotaBagian->Title;
    }

    public function To()
    {
        return $this->KeKota->Title . ' ' . $this->KeKotaBagian->Title;
    }

    public function SizeToString()
    {
        switch ($this->Size) {
            case 1:
                return 'Small';
                break;

            case 2:
                return 'Medium';
                break;

            case 3:
                return 'Large';
                break;

            default:
                return 'All size';
                break;
        }
    }

    public function TypeToString()
    {
        switch ($this->Type) {
            case 1:
                return 'Sameday';
                break;

            default:
                return 'Express';
                break;
        }
    }
}
