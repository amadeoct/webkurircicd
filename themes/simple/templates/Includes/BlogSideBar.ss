<div class="column column-1-4 cm-smart-column">
    <div class="cm-smart-column-wrapper">
        <ul class="categories clearfix">
            <% loop $getCategories %>
                <li><a href="$getLink">$Title<span>$countPosts</span></a></li>
            <% end_loop %>
        </ul>
        <ul class="services-list services-icons gray page-margin-top clearfix">
            <li>
                <a href="$getOrderLink" title="Cost Calculator">
                    <i class="fas fa-shipping-fast green" style="font-size: 7em;"></i>
                </a>
                <h4><a href="$getOrderLink" title="Cost Calculator">ORDER SHIPPING</a></h4>
                <p class="description">Ship it now.</p>
                <div class="align-center margin-top-40 padding-bottom-16">
                    <a class="more" href="$getOrderLink" title="Learn more">Click Here</a>
                </div>
            </li>
        </ul>
        <h6 class="box-header page-margin-top">LATEST POSTS</h6>
        <ul class="blog small margin-top-30 clearfix">
            <% if $getLatestPost %>
                <% loop $getLatestPost %>
                    <li>
                        <a href="$getLink" class="post-image">
                            <% if $Image %>
                                <img src="$Image.Fill(90,90).AbsoluteURL" alt="">
                            <% else %>
                                <img src="images/samples/90x90/placeholder.jpg" alt="">
                            <% end_if %>
                        </a>
                        <div class="post-content">
                            <a href="$getLink">$Title</a>
                            <ul class="post-details">
                                <li class="date">$Created.Month $Created.DayOfMonth, $Created.Year</li>
                            </ul>
                        </div>
                    </li>
                <% end_loop %>
            <% end_if %>
        </ul>
        <h6 class="box-header page-margin-top">TODAYS QUOTE</h6>
        <p class="margin-top-10">$SiteConfig.TodaysQuote</p>
        <h6 class="box-header page-margin-top">TAGS</h6>
        <ul class="taxonomies margin-top-30 clearfix">
            <% if $getTags %>
                <% loop $getTags %>
                    <li><a href="$getLink">$Title</a></li>
                <% end_loop %>
            <% end_if %>
        </ul>
    </div>
</div>
