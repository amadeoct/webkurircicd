<?php

use SilverStripe\Security\PasswordValidator;
use SilverStripe\Security\Member;

// remove PasswordValidator for SilverStripe 5.0
$validator = PasswordValidator::create();
// Settings are registered via Injector configuration - see passwords.yml in framework
Member::set_password_validator($validator);

date_default_timezone_set('Asia/Jakarta');


//remove menu on admin===========================
use SilverStripe\Admin\CMSMenu;
use SilverStripe\Reports\ReportAdmin;
use SilverStripe\VersionedAdmin\ArchiveAdmin;
use SilverStripe\CampaignAdmin\CampaignAdmin;
use SilverStripe\AssetAdmin\Controller\AssetAdmin;

CMSMenu::remove_menu_class(ReportAdmin::class);
// CMSMenu::remove_menu_class(AppsAdmin::class);
CMSMenu::remove_menu_class(AccessTokenAdmin::class);
CMSMenu::remove_menu_class(ArchiveAdmin::class);
CMSMenu::remove_menu_class(CampaignAdmin::class);
CMSMenu::remove_menu_class(AssetAdmin::class);
// CMSMenu::remove_menu_class(SecurityAdmin::class);
