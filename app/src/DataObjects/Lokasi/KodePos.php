<?php

use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class KodePos extends DataObject
{
    /**
     * Singular name for CMS
     *  @var string
     */
    private static $singular_name = 'KodePos';

    /**
     * Plural name for CMS
     *  @var string
     */
    private static $plural_name = 'KodePos';

    private static $db = [
        'Kode' => 'Int'
    ];

    private static $has_one = [
        'Kecamatan' => Kecamatan::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'Kode',
        'Kecamatan.Title' => 'Kecamatan',
    ];
}
