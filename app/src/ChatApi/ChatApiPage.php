<?php


use SilverStripe\Dev\Debug;
use SilverStripe\Control\Director;
use SilverStripe\Security\Member;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\DB;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPResponse_Exception;
use SilverStripe\Assets\Upload;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;

class ChatApiPage extends ApiPage {

    function requireDefaultRecords() {
        if (!DataObject::get_one('ChatApiPage')) {
            $page = new ChatApiPage();
            $page->Title = 'Chat Api';
            $page->URLSegment = 'chat-api';
            $page->Status = 'Published';
            $page->write();
            $page->publish('Stage', 'Live');
            $page->flushCache();
            DB::alteration_message('ChatApiPage created on page tree', 'created');
        }
        parent::requireDefaultRecords();
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName([
            'Content'
        ]);
        return $fields;
    }
}


class ChatApiPageController extends ApiPageController
{
    private static $allowed_actions = [
        'index',
        'send',
        'fcmidregistration',
        'read',
        'delete',
        'listroom',
        'getchat',
        'deleteroom'
    ];

    public function index(HTTPRequest $request){
        $this->response->addHeader('Content-Type', 'application/json');
        echo json_encode([
            'Status' => '200',
            'Message' => 'OK',
        ]);
        return;
    }

    public function getchat(){
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $OrderID = isset($_REQUEST['OrderID']) ? $_REQUEST['OrderID'] : "";
        $Keyword = isset($_REQUEST['Keyword']) ? $_REQUEST['Keyword'] : "";
        $Start = isset($_REQUEST['Start']) ? $_REQUEST['Start'] : 0;
        $Count = isset($_REQUEST['Count']) ? $_REQUEST['Count'] : 10;

        if ($OrderID == ""){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'OrderID Required',
            ]);
            return;
        }
        $order = Order::get_by_id($OrderID);
        if (!$order) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'Order not found'
            ]);
            return;
        }
        if ($order->CustomerID != $member->ID && $order->KurirID != $member->ID) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'This is not yours'
            ]);
            return;
        }

        $chat = ChatData::get()->filter('OrderID', $order->ID);
        $result = [];
        foreach ($chat as $key => $value) {
            $result[] = $value->toArray();
        }

        $this->response->addHeader('Content-Type', 'application/json');
        echo json_encode([
            'Status' => 200,
            'Message' => 'Success',
            'Data' => $result
        ]);
        return;

    }

    public function listroom(){
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $keyword = isset($_REQUEST['Keyword']) ? $_REQUEST['Keyword'] : "";
        $Start = isset($_REQUEST['Start']) ? $_REQUEST['Start'] : 0;
        $Count = isset($_REQUEST['Count']) ? $_REQUEST['Count'] : 10;

        //get list room
        $orders = Order::get()->filter([
            'KurirID' => $member->ID,
            'StatusPengiriman:LessThan' => 3,
            'Type' => 0
        ]);
        $result = [];
        foreach ($orders as $key => $value) {
            $room = [];
            $room['ID'] = $value->ID;
            $room['UserName'] = $value->Customer()->FirstName . ' ' . $value->Customer()->Surname;
            $room['UserImageURL'] = $value->Customer()->Image()->AbsoluteURL;
            $room['UnreadChat'] = count(ChatData::get()->filter([
                'ReceiverID' => $member->ID,
                'OrderID' => $value->ID,
                'HasRead' => 0
            ]));
            $lastchat = ChatData::get()->filter('OrderID', $value->ID)->sort('ID DESC')->first();
            if ($lastchat) {
                $room['LastChat'] = $lastchat->toArray();
                $result[] = $room;
            }
        }

        $this->response->addHeader('Content-Type', 'application/json');
        echo json_encode([
            'Status' => 200,
            'Message' => 'Success',
            'Data' => $result
        ]);
        return;


    }



    public function read(){
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $OrderID = isset($_REQUEST['OrderID']) ? $_REQUEST['OrderID'] : "";

        if ($OrderID == ""){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'Order ID Required',
            ]);
            return;
        }

        $order = Order::get_by_id($OrderID);
        if (!$order) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'Order not found'
            ]);
            return;
        }
        if ($order->CustomerID != $member->ID && $order->KurirID != $member->ID) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'This is not yours'
            ]);
            return;
        }

        $chats = ChatData::get()->filter([
            'OrderID' => $OrderID,
            'ReceiverID' => $member->ID
        ]);

        if ($chats->count() != 0){

            //read all chat from requessted line chat
            foreach ($chats as $c){
                $c->HasRead = 1;
                $c->write();
            }

            $chats = $chats->first();
            $receiver = $chats->Sender();
            if ($chats->SenderID == $member->ID) {
                $receiver = $chats->Receiver();
            }

            $fcmReceiver = FCMID::getMemberToken($receiver);
            $data = $chats->toArray();
            $data['Context'] = "READ";
            $data['Title'] = "Someone Read your message";
            $send = Notifier::Notify($fcmReceiver, $data, ['Context' => 'READ']);
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 200,
                'Message' => 'Success Readed Chats -'.$chats->ID,
                'Data' => $chats->toArray(),
                'FcmLog' => $send,
                'FCM_TARGET' => $fcmReceiver
            ]);
            return;
        }else{
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'Chats Not found',
            ]);
        }

    }

    // Discontinued
    public function deleteroom(){
        return $this->HttpError('404');

        $memberID = isset($_REQUEST['MemberID']) ? $_REQUEST['MemberID'] : "";
        $RoomCode = isset($_REQUEST['RoomCode']) ? $_REQUEST['RoomCode'] : "";

        if ($RoomCode == "" || $memberID == ""){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'RoomCode | MemberID is required',
            ]);
            return;
        }

        $Chats = ChatData::get()->filter([
            'RoomCode' => $RoomCode,
            'OwnerDataID' => $memberID
        ]);

        if ($Chats->count() == 0){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'Chat Not Found',
            ]);
            return;
        }

        foreach($Chats as $c){
            $c->delete();
        }

        echo json_encode([
            'Status' => 200,
            'Message' => 'Success Delete Room Chat',
        ]);
        return;



    }

    public function delete(){
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $ID = isset($_REQUEST['ID']) ? $_REQUEST['ID'] : "";
        $ForEveryone = isset($_REQUEST['ForEveryone']) ? $_REQUEST['ForEveryone'] : 0;

        $ID = json_decode($ID, true);

        $Chats = ChatData::get()->filter($ID);
        if ($Chats->count() == 0){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 404,
                'Message' => 'Chat Not Found',
            ]);
            return;
        }

        foreach ($Chats as $key => $value) {
            if ($value->SenderID != $member->ID && $value->ReceiverID != $member->ID) {
                $this->response->addHeader('Content-Type', 'application/json');
                echo json_encode([
                    'Status' => 404,
                    'Message' => 'This is not your',
                ]);
                return;
            }
            if ($ForEveryone == 1 && $value->SenderID != $memberID) {
                $this->response->addHeader('Content-Type', 'application/json');
                echo json_encode([
                    'Status' => 404,
                    'Message' => 'Can not delete chat for everyone, you\'re not the sender',
                ]);
                return;
            }
        }

        foreach ($Chats as $key => $value) {
            if ($ForEveryone == 1) {
                $value->Visibility == 4;
            }
            else {
                if ($value->SenderID == $member->ID) {
                    if ($value->Visibility == 0) {
                        $value->Visibility = 2;
                    }
                    else {
                        $value->Visibility = 3;
                    }
                }
                else {
                    if ($value->Visibility == 0) {
                        $value->Visibility = 1;
                    }
                    else {
                        $value->Visibility = 3;
                    }
                }
            }
            $value->write();
        }
    }


    public function fcmidregistration(){
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $id = isset($_REQUEST['FCMID']) ? $_REQUEST['FCMID'] : "";
        $memberID = isset($_REQUEST['MemberID']) ? $_REQUEST['MemberID'] : "";
        if ($id == "" || $memberID == ""){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'FCMID & MemberID Required',
            ]);
            return;
        }
        try {
            $newFCMID = new FCMID();
            $newFCMID->Token = $id;
            $newFCMID->MemberID = $memberID;
            $newFCMID->write();

            // $result = Notifier::Notify([$id], ['Title' => 'Sukses', 'Content' => 'Testing Register FCMID Sukses']);

            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 200,
                'Message' => 'success register FCMID',
                // 'FCMResult' => $result
            ]);
            return;
        } catch (\Throwable $th) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 500,
                'Message' => $th,
            ]);
            return;
        }
        return;
    }

    public function send(){
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $OrderID = isset($_REQUEST['OrderID']) ? $_REQUEST['OrderID'] : "";
        $content = isset($_REQUEST['Content']) ? $_REQUEST['Content'] : "";
        $ImageID = 0;

        if ($OrderID == "" || $content == ""){
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'OrderID | Content is required',
            ]);
            return;
        }

        $order = Order::get_by_id($OrderID);
        if (!$order) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'Order not found',
            ]);
            return;
        }

        if ($order->KurirID != $member->ID && $order->CustomerID != $member->ID) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 417,
                'Message' => 'This order is not yours',
            ]);
            return;
        }

        if (isset($_REQUEST['Image'])){
            $file = new Image();
            $upload = new Upload();
            $upload->loadIntoFile($_POST['Image'], $file);
            $ImageID = $file->ID;
        }

        $receiver = $order->Kurir();
        if ($receiver->ID == $member->ID) {
            $receiver = $order->Customer();
        }

        // var_dump($receiver);die();

        // save chat to sender
        $SenderNewChat = new ChatData();
        $SenderNewChat->OrderID = $order->ID;
        $SenderNewChat->SenderID = $member->ID;
        $SenderNewChat->ReceiverID = $receiver->ID;
        $SenderNewChat->Content = $content;
        $SenderNewChat->ImageID = $ImageID;
        $SenderNewChat->write();

        // $member = Member::get_by_id($receiver);
        // $member->HasNewChat = 1;
        // $member->write();

        $chats = ChatData::get()->filter([
            'OrderID' => $OrderID,
            'ReceiverID' => $receiver->ID,
            'HasRead' => 0
        ]);

        try {
            //send data to receiver
            $fcmReceiver = FCMID::getMemberToken($SenderNewChat->Receiver());
            $data = $SenderNewChat->toArray();
            // Debug::show($fcmReceiver);
            $data['Context'] = "CHAT";
            $data['UnreadChat'] = count($chats);
            $send = "{}";
            if ( !empty($fcmReceiver) ){
                $send = Notifier::Notify($fcmReceiver, $data, ['Context' => 'CHAT']);
            }
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 200,
                'Message' => "Success send message",
                'Data' => $SenderNewChat->toArray(),
                'FcmLog' => $send,
                'FCM_TARGET' => $fcmReceiver
            ]);
            return;
        } catch (\Throwable $th) {
            $this->response->addHeader('Content-Type', 'application/json');
            echo json_encode([
                'Status' => 500,
                'Message' => $th,
            ]);
            return;
        }
    }

    public function unreadchats()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $chats = ChatData::get()->filter([
            'ReceiverID' => $member->ID
        ]);

        $count = 0;
        foreach ($chats as $key => $value) {
            if ($value->HasRead == 0) {
                $count++;
            }
        }

        $this->response->addHeader('Content-Type', 'application/json');
        echo json_encode([
            'Status' => 200,
            'Message' => "Success count unread chat",
            'Count' => $count
        ]);
        return;
    }
}
