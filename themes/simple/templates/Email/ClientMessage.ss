<html>
    <body  style="background-color: #5db96b">
        <div class="container" style="background-color: white; padding: 50px; margin-left: 100px; margin-right: 100px">
            <h1><img src="$Config.Logo.AbsoluteURL" width="150px" height="150px">$Config.Title</h1>
            <h4 style="color: red">Message From Client</h4>
            <hr>
            <table style="border-collapse: collapse">
                <tbody>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>$Name</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>$Email</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>:</td>
                        <td>$Phone</td>
                    </tr>
                    <tr>
                        <td>Message</td>
                        <td>:</td>
                        <td>$Message</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            Best Regards,
            <br>
            <br>
            <br>
            $Config.Title
        </div>
    </body>
</html>
