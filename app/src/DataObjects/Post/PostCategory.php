<?php

use SilverStripe\Control\Director;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class PostCategory extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(150)',
        'URLSegment' => 'Varchar(200)',
    ];

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function onBeforeWrite()
    {
        $urlSegment = $this->clean($this->Title);
        $count = count(PostCategory::get()->filter(['URLSegment:PartialMatch'=>$urlSegment]));
        if ($count > 1) {
            $urlSegment .= '-' . ($count+1);
        }
        $this->URLSegment = $urlSegment;
	    parent::onBeforeWrite();
    }

    public function getCMSFields()
    {
        $field = FieldList::create(
            TextField::create(
                'Title',
                'Title'
            )
        );
        return $field;
    }

    public function getLink()
    {
        $page = BlogPage::get()->first();
        if ($page) {
            return $page->AbsoluteLink() . '?category=' .  $this->URLSegment;
        }
        return '';
    }

    public function countPosts()
    {
        return count(Post::get()->filter('CategoryID', $this->ID));
    }
}
