<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\ORM\PaginatedList;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class BlogPage extends Page
{
    public function getCategories()
    {
        return PostCategory::get();
    }

    public function getTags()
    {
        return PostTag::get();
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class BlogPageController extends PageController
{
    public function doInit()
    {
        parent::doInit();
    }

    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'Detail'
    ];

    public function index()
    {
        $category = isset($_REQUEST['category']) ? $_REQUEST['category'] : null;
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : null;
        $post = Post::get();
        if ($category) {
            $post = $post->filter('Category.URLSegment', $category);
        }
        if ($tag) {
            $post = $post->filter('Tags.URLSegment', $tag);
        }

        $paginatedPost = PaginatedList::create(
            $post,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('page');
        return ['Post' => $paginatedPost];
    }

    public function Detail()
    {
        $urlsegment = $this->getRequest()->param('ID');
        $post = Post::get()->filter('URLSegment', $urlsegment)->first();
        return ['Post' => $post];
    }
}
