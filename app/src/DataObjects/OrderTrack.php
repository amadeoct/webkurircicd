<?php

use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class OrderTrack extends DataObject
{
    private static $db = [
        'Title' => 'Text',
        'Timestamp' => 'Datetime'
    ];

    private static $has_one = [
        'Order' => Order::class,
        // 'Proof' => Image::class
    ];

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['Title'] = $this->Title;
        $arr['Timestamp'] = date('j M H:i', strtotime($this->Timestamp));
        $arr['OrderID'] = $this->OrderID;
        // $arr['ProofURL'] = $this->Proof()->AbsoluteURL;
        return $arr;
    }
}
