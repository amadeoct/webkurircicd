<?php

use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TabSet;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Apps extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(255)',
        'ClientID' => 'Varchar(100)',
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Name',
        'ClientID',
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->ID == 0) {
            $this->ClientID = md5(rand(0, 100) . 'WebKurir' . rand(0, 100) . 'Live');
        }
    }

    public function getCMSFields()
    {
        $fields = FieldList::create();
        $fields->add(new TabSet("Root"));

        $fields->addFieldToTab(
            'Root.Main',
            TextField::create(
                'Title',
                'Title'
            )
        );

        if ($this->ID != 0) {
            $fields->addFieldToTab(
                'Root.Main',
                ReadonlyField::create(
                    'ClientID',
                    'ClientID'
                )
            );
        }

        return $fields;
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class AppsAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'Apps'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'apps-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Apps';


}
