<?php





use SilverStripe\Control\Director;

use SilverStripe\SiteConfig\SiteConfig;

use SilverStripe\Security\Member;

use SilverStripe\ORM\DataObject;



class FCMID extends DataObject {



    public function canCreate($member = null, $context = [])
    {
        return false;
    }



    public function canDelete($member = null)
    {
        return false;
    }



    public function canEdit($member = null)
    {
        return false;
    }

    private $max_device = 998;



    private static $db = [
        'Token' => 'Text',
    ];



    private static $has_one = [
        'Member' => Member::class
    ];



    private static $summary_fields = [
        'Token' => 'Token',
        'Member.Email' => 'Member'
    ];



    static function getMemberToken($member){
        $result = [];
        $data = FCMID::get()->filter([
            'MemberID' => $member->ID
        ])->sort("ID", "DESC")->limit(1);
        foreach($data as $d){
            if (!in_array($d->Token, $result)){
                array_push($result, $d->Token);
            }
        }
        return $result;
    }

    public function toArray(){
        $data = [];
        $data['ID'] = $this->ID;
        $data['MemberID'] = $this->MemberID;
        $data['Token'] = $this->Token;
        return $data;
    }

    
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        $other = FCMID::get()->filter([
            'MemberID' => $this->MemberID,
        ])->sort("ID", "ASC");
        // $delete unused ids
        if ($other->count() >= $this->max_device){
            $maxIter = $other->count() - $this->max_device + 1;
            $count = 0;
            foreach ($other as $o){
                $o->delete();
                $count += 1;
                if ($count == $maxIter){
                    break;
                }
            }
        }
    }

}