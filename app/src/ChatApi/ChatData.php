<?php
use SilverStripe\Control\Director;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Security\Member;
use SilverStripe\ORM\DataObject;
use SilverStripe\Dev\Debug;
class ChatData extends DataObject {

     private static $db = [
        'Content' => 'HTMLText',
        'MsgType' => 'Varchar(255)',
        'HasRead' => 'Boolean',
        'Visibility' => 'Int' // 0 Vilible Both, 1 Only Sender, 2 Only Receiver, 3 Both Not Visible, 4 Unsent
    ];

    private static $has_one = [
        'Order' => Order::class,
        'Sender' => Member::class,
        'Receiver' => Member::class,
        'Image' => ImageCustom::class
    ];

    public function getImageHandled(){
        if ($this->ImageID == 0){
            return "";
        }
        return Director::absoluteBaseURL().$this->Image()->imageHandled()->Filename;
    }

    public function toArray(){
        $data = [];
        $data['ID'] = $this->ID;
        $data['OrderID'] = $this->OrderID;
        $data['ClassName'] = $this->ClassName;
        $data['Title'] = "Message from ".$this->Sender()->FirstName;
        $created = date('m/d/Y', strtotime($this->Created));
        $today = date('m/d/Y');
        $yesterday = date('m/d/Y', strtotime("-1 days"));
        $data['Created'] = $created;
        if ($created == $today) {
            $data['Created'] = date('g:i a', strtotime($this->Created));
        }
        elseif ($created == $yesterday) {
            $data['Created'] = 'Yesterday';
        }
        $data['LastEdited'] = $this->LastEdited;
        $data['MsgType'] = $this->MsgType;
        $data['Content'] = $this->Content;
        $data['HasRead'] = $this->HasRead;
        $data['SenderID'] = $this->SenderID;
        $data['Sender'] = $this->Sender()->toArray();
        $data['ReceiverID'] = $this->ReceiverID;
        $data['Receiver'] = $this->Receiver()->toArray();
        $data['Image'] = $this->getImageHandled();
        return $data;
    }

    public function onBeforeWrite(){
        parent::onBeforeWrite();
        if ($this->ID == 0){
            $this->HasRead = 0;
            if ($this->FileID == 0 && $this->ImageID == 0){
                $this->MsgType = "FULL_TEXT";
            }else{
                $this->MsgType = "MEDIA";
            }
            // if ($this->OwnerDataID == $this->SenderID){
            //         $this->OwnerPosition = "SENDER";
            // }else{
            //         $this->OwnerPosition = "RECEIVER";
            // }
        }
    }
}
