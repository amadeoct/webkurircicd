var CACHE_NAME = 'web-kurir';
var urlsToCache = [
  '/',
  '/public/',
  '/public/offline.html'
];


self.addEventListener('install', function(event) {
  console.log('[Service Worker] Install');

  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  console.log(`[Service Worker] Fetched resource ${event.request.url}`);
//   event.respondWith(
//     caches.match(event.request)
//       .then(function(response) {
//         // Cache hit - return response
//         if (response) {
//           return response;
//         }
//         return fetch(event.request);
//       }
//     )
//   );
  event.respondWith(
    fetch(event.request).then(function(response) {
      return response;
    }).catch(function() {
      return new Response("Uh oh, no internet connection!");
    })
  );
});

self.addEventListener('activate', function(event) {
  console.log('[Service Worker] Activate');

//   var loadingTime = 0;
//   let timer = setInterval(() => {
//     loadingTime++;
//   }, 1000);
//   document.onload = () => {
//     clearInterval(timer);
//     alert('Done');
//   }


  var cacheWhitelist = ['web-kurir'];
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (cacheWhitelist.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('message', function(event) {
  console.log(`[Service Worker] Message`);
});
