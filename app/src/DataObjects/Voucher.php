<?php

use SilverStripe\Forms\DropdownField;
use SilverStripe\Security\Member;
use SilverStripe\Dev\Debug;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\HTMLEditor\HtmlEditorField;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Voucher extends DataObject
{
    private static $db = [
        'Kode' => 'Varchar(150)',
        'Title' => 'Varchar(255)',
        'Description' => 'HTMLText',
        'Jenis' => 'Varchar(10)',
        'Potongan' => 'Int',
        'Valid' => 'Date',
        'Expires' => 'Date',
        'isMinimumPembelian' => 'Boolean',
        'MinimumPembelian' => 'Int',
        'isMaximumPotongan' => 'Boolean',
        'MaximumPotongan' => 'Int',
        'isLimitPemakaian' => 'Boolean',
        'LimitPemakaian' => 'Int',
        'isQuota' => 'Boolean',
        'Quota' => 'Int',
        'isInCity' => 'Boolean'
    ];

    private static $has_one = [
        'Image' => Image::class
    ];

    private static $has_many = [
        'City' => VoucherManyCity::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'Kode',
        'Title',
        'Description',
        'Expires'
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        $this->Kode = strtoupper($this->Kode);
        if ($this->Image->exists() && !$this->Image->isPublished()) {
            $this->Image->doPublish();
        }

        if (isset($_POST['Valid'])) {
            $this->Valid = $_POST['Valid'];
        }

        if (isset($_POST['Expires'])) {
            $this->Expires = $_POST['Expires'];
        }
    }

    /**
     * Event handler called after writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onAfterWrite()
    {
        parent::onAfterWrite();
        if (isset($_REQUEST['City'])) {
            $city = $_REQUEST['City'];
            $voucher_many_city = VoucherManyCity::get()->filter('VoucherID', $this->ID);
            foreach ($voucher_many_city as $key => $value) {
                $value->delete();
            }

            foreach ($city as $key => $value) {
                $vmc = VoucherManyCity::create();
                $vmc->VoucherID = $this->ID;
                $vmc->KotaID = $value;
                $vmc->write();
            }
        }
    }

    public function getCMSFields()
    {
        // Requirements::css('https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.css');
        // Requirements::javascript('https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.js');
        Requirements::javascript('VoucherJS.js');

        $fields = parent::getCMSFields();
        $arr = [];
        foreach (Voucher::$db as $key => $value) {
            $arr[] = $key;
        }
        $fields->removeByName($arr);
        $fields->removeByName('City');

        $jenis = OptionsetField::create(
            'Jenis',
            'Jenis Potongan',
            [
                'Rupiah' => 'Rp',
                'Persen' => '%'
            ]
        );

        if($this->ID) {
            $jenis = ReadonlyField::create(
                'Jenis',
                'Jenis Potongan'
            );
        }

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create(
                    'Kode',
                    'Kode'
                ),
                TextField::create(
                    'Title',
                    'Title'
                ),
                HtmlEditorField::create(
                    'Description',
                    'Description'
                ),
                $jenis,
                TextField::create(
                    'Potongan',
                    'Potongan'
                ),
                LiteralField::create('DateField', '
                    <style>
                        .input {
                            padding: 5px 2px;
                            border: 1px solid #b3b3b3;
                            border-radius: 4px;
                        }
                    </style>
                    <div class="form-group field">
                        <label class="form__field-label">Valid Date</label>
                        <div class="form__field-holder">
                            <input type="date" class="input" name="Valid" id="FieldValid" value="'.$this->Valid.'" onchange="changeValid(this)">
                            s/d
                            <input type="date" class="input" name="Expires" id="FieldExpires" value="'.$this->Expires.'" onchange="changeExpires(this)">
                            <p class="form__field-description form-text">Masukkan tanggal yang sama jika voucher berlaku dalam 1 hari saja</p>
                        </div>
                    </div>
                ')
            ]
        );

        $option = '';
        $city = Kota::get();
        foreach ($city as $key => $value) {
            $option .= '<option value="'.$value->ID.'"';

            if ($this->isCityConnected($value->ID)) {
                $option .= ' selected';
            }

            $option .= '>'.$value->Title.'</option>';
        }

        $fields->addFieldsToTab(
            'Root.Rules',
            [
                LiteralField::create('HeaderIsMinimumPembelian','
                    <h1>Gunakan minimum pembelian?</h1>
                    <p>Voucher hanya dapat dipakai jika total pembelian lebih dari minimum pembelian</p>
                '),
                CheckboxField::create(
                    'isMinimumPembelian',
                    'Gunakan minimum pembelian'
                ),
                NumericField::create(
                    'MinimumPembelian',
                    'Minimum Pembelian'
                )->setDescription('Jumlah minimum pembelian akan diabaikan jika checkbox diatas tidak tercentang'),
                LiteralField::create('HeaderIsMaximumPotongan','
                    <h1>Gunakan maximum potongan?</h1>
                    <p>Voucher memotong tidak lebih dari maximum potongan</p>
                '),
                CheckboxField::create(
                    'isMaximumPotongan',
                    'Gunakan maximum potongan'
                ),
                NumericField::create(
                    'MaximumPotongan',
                    'Maximum Potongan'
                )->setDescription('Jumlah maximum potongan akan diabaikan jika checkbox diatas tidak tercentang'),
                LiteralField::create('HeaderLimitPemakaian','
                    <h1>Gunakan limit pemakaian?</h1>
                    <p>Limit pemakaian voucher pada masing - masing member</p>
                '),
                CheckboxField::create(
                    'isLimitPemakaian',
                    'Gunakan limit pemakaian'
                ),
                NumericField::create(
                    'LimitPemakaian',
                    'Limit Pemakaian'
                )->setDescription('Jumlah limit pemakaian akan diabaikan jika checkbox diatas tidak tercentang'),
                LiteralField::create('HeaderQuota','
                    <h1>Gunakan quota voucher?</h1>
                    <p>Quota pemakaian voucher</p>
                '),
                CheckboxField::create(
                    'isQuota',
                    'Gunakan quota voucher'
                ),
                NumericField::create(
                    'Quota',
                    'Quota'
                )->setDescription('Jumlah quota akan diabaikan jika checkbox diatas tidak tercentang'),
                LiteralField::create('HeaderCity','
                    <h1>Untuk kota tertentu?</h1>
                    <p>Voucher dapat digunakan untuk pengiriman di area kota tertentu</p>
                '),
                CheckboxField::create(
                    'isInCity',
                    'Untuk kota tertentu'
                ),
                LiteralField::create('SelectCity','
                    <div class="form-group field">
                        <label class="form__field-label">Pilih Kota</label>
                        <div class="form__field-holder">
                            <select id="city" name="City[]" multiple>
                                '.$option.'
                            </select>
                            <p class="form__field-description form-text">Voucher hanya dapat digunakan untuk pengiriman di area kota tertentu</p>
                        </div>
                    </div>
                '),
            ]
        );

        return $fields;
    }

    public function isCityConnected($id)
    {
        $vmc = VoucherManyCity::get()->filter([
            'VoucherID' => $this->ID,
            'KotaID' => $id
        ]);

        if (count($vmc) > 0) {
            return true;
        }
        return false;
    }

    public function AvailableQuota()
    {
        // TODO: Get Orders That Using This Voucher
        $quota = $this->Quota;
        $terpakai = Order::get()->filter([
            'VoucherID' => $this->ID
        ]);

        return $quota - count($terpakai);
    }

    public function AvailableLimit($member = null)
    {
        // TODO: Get Member Orders That Using This Voucher
        if ($member == null) {
            return 99;
        }
        $limit = $this->LimitPemakaian;
        $terpakai = Order::get()->filter([
            'VoucherID' => $this->ID,
            'CustomerID' => $member->ID
        ]);

        return $limit - count($terpakai);
    }

    public function toArray()
    {
        $arr = array();
        $arr['ID'] = $this->ID;
        $arr['Created'] = date('j F Y', strtotime($this->Created));
        $arr['Valid'] = date('j F Y', strtotime($this->Valid));
        $arr['Expires'] = date('j F Y', strtotime($this->Expires));
        $arr['Code'] = $this->Kode;
        $arr['Title'] = $this->Title;
        $arr['Description'] = $this->Description;
        $arr['Jenis'] = $this->Jenis;
        $arr['Potongan'] = $this->Potongan;
        $arr['ImageURL'] = $this->Image()->AbsoluteURL;

        return $arr;
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class VoucherAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'Voucher'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'voucher-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Voucher';

    private static $menu_icon_class = 'font-icon-tags';
}
