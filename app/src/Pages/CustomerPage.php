<?php

use SilverStripe\ORM\ArrayList;
use SilverStripe\Assets\Upload;
use SilverStripe\Assets\Image;
use SilverStripe\Security\Member;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Security\Security;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use SilverStripe\Control\Session;
use SilverStripe\Control\Email\Email;
use SilverStripe\ORM\PaginatedList;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class CustomerPage extends Page
{
    public function CountPendingOrder()
    {
        $customer = Member::currentUser();
        $order = Order::get()->filter(['CustomerID' => $customer->ID])->filter('StatusPengiriman:LessThan', 2);
        return count($order);
    }

    public function CountOnGoingOrder()
    {
        $customer = Member::currentUser();
        $order = Order::get()->filter(['CustomerID' => $customer->ID, 'StatusPengiriman' => 2]);
        return count($order);
    }

    public static function SendEmail($customer)
    {
        $config = SiteConfig::current_site_config();
        $url = Director::absoluteBaseURL();
        $from = 'no-reply@webkurir.com';
        $to = $customer->Email;
        $link = CustomerPage::get()->first()->AbsoluteLink() . 'VerifyMe/' . $customer->ID;
        $subject = $config->Title . ' - Email Verification';
        if (substr($url, 0, 16) != 'http://localhost') {
            // SMTP Configuration
            $transport = (new Swift_SmtpTransport($config->SMTPHost, $config->SMTPPort,'tls'))
            ->setUsername($config->SMTPUsername)
            ->setPassword($config->SMTPPassword);
            $mailer = new Swift_Mailer($transport);
            $message = (new Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                '<html>' .
                ' <body  style="background-color: #5db96b">' .
                '   <div class="container" style="background-color: white; padding: 50px;">' .
                '     <h1><img src="' . $config->Logo->AbsoluteURL .'" width="150px" height="150px"> ' . $config->Title . '</h1>' .
                '     <h4 style="color: red">Email Confirmation</h4>' .
                '     <hr>' .
                '     <h3>Halo,</h3>' .
                '     Terima Kasih telah melakukan registrasi di ' . $config->Title . '<br>' .
                '     Harap Klik Tombol dibawah untuk melanjutkan Registrasi '.
                '     <br>' .
                '     <a href="' . $link . '">' .
                '       <button class="btn" style="border-radius: 50px; background-color: #5db96b">Click Here</button>' .
                '     </a><br>' .
                '     <sub>' .
                '       *If you didnt recently attempt to create an account with this email address, you can safely ignore this email.' .
                '     </sub>' .
                '     <br>' .
                '     <br>' .
                '       Best Regards,<br>' .
                '     <br>' .
                '     <br>' .
                '       '. $config->Title .'.' .
                '   </div>' .
                ' </body>' .
                '</html>',
                'text/html' // Mark the content-type as HTML
            );
            $result = $mailer->send($message);
        }
        else {
            $email = Email::create()
            ->setData([
                'Link'=> $link,
            ])
            ->setHTMLTemplate('Email\\CustomerRegistration')
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);
            $email->send();
        }
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class CustomerPageController extends PageController
{
    public function doInit()
    {
        parent::doInit();
    }

    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'Dashboard',
        'Register',
        'ProsesRegister',
        'Registered',
        'ResendEmail',
        'VerifyMe',
        'VerifyEmail',
        'ForgotPassword',
        'SendForgotEmail',
        'RepairPassword',
        'ProsesrepairPassword',
        'GoogleLogin',
        'Login',
        'ProsesLogin',
        'EditProfile',
        'UploadPhoto',
        'ProsesEditProfile',
        'Logout',
        'PendingOrder',
        'Bayar',
        'CancelOrder',
        'OnGoingOrder',
        'HistoryOrder',
        'Rate',
        'GetVoucher'
    ];

    public function index()
    {
        $page = CustomerPage::get()->first();
        return $this->redirect($page->Link() . 'Dashboard');
    }

    public function Dashboard()
    {
        $page = CustomerPage::get()->first();
        $customer = Member::currentUser();
        if (!$customer) {
            return $this->redirect($page->Link() . 'Login');
        }
        if ($customer->Status == 1) {
            return $this->redirect($page->Link() . 'Registered');
        }
        return ['Customer' => $customer, 'BC' => 'Dashboard'];
    }

    public function Register()
    {
        $Flash =  $this->getRequest()->getSession()->get('Flash');
        if ($Flash != null) {
            $this->getRequest()->getSession()->clear('Flash');
        }
        return ['Flash' => $Flash];
    }

    public function ProsesRegister()
    {
        $session = $this->getRequest()->getSession();
        $firstname = $_POST['FirstName'];
        $surname = $_POST['Surname'];
        $notelp = $_POST['NoTelp'];
        $email = $_POST['Email'];
        $password = $_POST['Password'];
        $cpassword = $_POST['CPassword'];
        if (!$email || !$password || !$cpassword || !$firstname || !$surname || !$notelp) {
            $session->set('Flash', 'Mohon isi semua kotak yang kosong');
            return $this->RedirectBack();
        }

        $customer = Member::get()->filter('Email', $email)->first();
        if ($customer) {
            $session->set('Flash', 'Email sudah terdaftar');
            return $this->RedirectBack();
        }

        if (strlen($password) < 8) {
            $session->set('Flash', 'Password harus minimal 8 karakter');
            return $this->RedirectBack();
        }

        if ($password != $cpassword) {
            $session->set('Flash', 'Password dan Confirmation Password harus sama');
            return $this->RedirectBack();
        }

        $customer = Customer::create();
        $customer->FirstName = $firstname;
        $customer->Surname = $surname;
        $customer->NoTelp = $notelp;
        $customer->Email = $email;
        $customer->Password = $password;
        $customer->write();

        CustomerPage::SendEmail($customer);

        $identityStore = Injector::inst()->get(IdentityStore::class);
        $identityStore->logIn($customer);

        $page = CustomerPage::get()->first();
        return $this->redirect($page->Link() . 'Registered');
    }

    public function Registered()
    {
        $customer = Member::currentUser();
        if ($customer->ClassName == 'Customer') {
            if ($customer->Status == 1) {
                return ['Registered' => true];
            }
            $page = CustomerPage::get()->first();
            return $this->redirect($page->Link() . 'Dashboard');
        }
        return $this->redirect('');
    }

    public function ResendEmail()
    {
        $customer = Member::currentUser();
        if ($customer->ClassName == 'Customer') {
            if ($customer->Status == 1) {
                CustomerPage::SendEmail($customer);

                return json_encode([
                    'Status' => 200,
                    'Message' => 'Email Sent'
                ]);
            }
            return $this->redirect($page->Link() . 'Dashboard');
        }
        return $this->redirect('');
    }

    public function VerifyMe()
    {
        $page = CustomerPage::get()->first();
        $id = $this->getRequest()->param('ID');
        $member = Customer::get_by_id($id);
        if ($member) {
            $member->Status = 2;
            $member->write();
            return $this->redirect($page->Link() . 'Dashboard');
        }
        return $this->HttpError('404');
    }

    public function VerifyEmail()
    {
        $page = CustomerPage::get()->first();
        $id = $this->getRequest()->param('ID');
        $token = CustomToken::get()->filter('Title', $id)->first();
        if (!$token) {
            return [
                'Status' => 'Error',
                'Message' => 'Token is not valid'
            ];
        }
        if (!$token->isValid()) {
            return [
                'Status' => 'Error',
                'Message' => 'Token is not valid'
            ];
        }

        $member = Customer::get_by_id($token->MemberID);
        $member->Status = 2;
        $member->write();

        // var_dump($member->toArray());die();

        $token = CustomToken::get()->filter('MemberID', $member->ID);
        foreach ($token as $key => $value) {
            $value->delete();
        }

        $token = AccessToken::create();
        $token->MemberID = $member->ID;
        $token->DeviceID = '123456';
        $token->write();

        $arrMember = $member->toArray();
        $arrMember['AccessToken'] = $token->Token;

        $data = [
            'Title' => 'Verified',
            'Content' => 'Your email has been verified',
            'Context' => 'EMAIL_VERIFIED',
            'Member' => $arrMember
        ];
        Notifier::Notify(FCMID::getMemberToken($member), $data);

        return $this->customise([
            'Status' => 'Success',
            'Message' => 'Verifikasi sukses, silahkan lanjutkan proses registrasi pada halaman aplikasi'
        ])->renderWith('VerifyEmail');
    }

    public function GoogleLogin()
    {
        // IF
        $email = isset($_GET['Email']) ? $_GET['Email'] : '';
        $firstname = isset($_GET['FirstName']) ? $_GET['FirstName'] : '';
        $surname = isset($_GET['Surname']) ? $_GET['Surname'] : '';
        $customer = Customer::get()->filter('Email', $email)->first();

        if (!$customer) {
            $kurir = Kurir::get()->filter('Email', $email)->first();
            if ($kurir) {
                $session = $this->getRequest()->getSession();
                $session->set('Flash', 'Anda tidak bisa Login disini! Coba Login as Kurir');
                return $this->RedirectBack();
            }

            $customer = Customer::create();
            $customer->Email = $email;
            $customer->FirstName = $firstname;
            $customer->Surname = $surname;
            $customer->Status = 2;
            $customer->Password = '0977a0ffc69e3376e3aa26145b218697';
            $customer->write();
        }

        $identityStore = Injector::inst()->get(IdentityStore::class);
        $identityStore->logIn($customer, true);

        // var_dump(Member::currentUser());die();

        if (isset($_GET['GoTo'])) {
            if ($_GET['GoTo'] == 'order') {
                return $this->redirect('order');
            }
        }

        $page = CustomerPage::get()->first();
        return $this->redirect($page->Link() . 'Dashboard');
    }

    public function ForgotPassword()
    {
        return ['Forgot' => true];
    }

    public function SendForgotEmail()
    {
        $email = isset($_POST['Email']) ? $_POST['Email'] : null;
        if (!$email) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Mohon masukkan email Anda'
            ]);
        }

        $customer = Customer::get()->filter('Email', $email)->first();
        if (!$customer) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Email tidak ditemukan, gunakan email Anda yang terdaftar pada kami'
            ]);
        }

        $page = CustomerPage::get()->first();
        $config = SiteConfig::current_site_config();
        $url = Director::absoluteBaseURL();
        $from = 'no-reply@webkurir.com';
        $to = $customer->Email;
        $link = $page->AbsoluteLink() . 'RepairPassword/' . $customer->ID;
        $subject = $config->Title . ' - Repair Password';

        $email = Email::create()
        ->setData([
            'Link'=> $link,
            'FirstName' => $customer->FirstName,
            'Surname' => $customer->Surname,
            'Title' => $config->Title,
            'Image' => $config->Logo->AbsoluteURL
        ])
        ->setHTMLTemplate('Email\\ForgotPasswordEmail')
        ->setFrom($from)
        ->setTo($to)
        ->setSubject($subject);

        if (substr($url, 0, 16) != 'http://localhost') {
            // SMTP Configuration
            $transport = (new Swift_SmtpTransport($config->SMTPHost, $config->SMTPPort, 'tls'))
            ->setUsername($config->SMTPUsername)
            ->setPassword($config->SMTPPassword);
            $mailer = new Swift_Mailer($transport);
            $message = (new Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $email->render()->Body,
                'text/html' // Mark the content-type as HTML
            );
            $result = $mailer->send($message);
        }
        else {
            $email->send();
        }

        return json_encode([
            'Status' => 200,
            'Message' => 'Sukses, Silahkan cek email kamu untuk melanjutkan proses repair password'
        ]);
    }

    public function RepairPassword()
    {
        $id = $this->getRequest()->param('ID');
        $page = CustomerPage::get()->first();

        if (!$id) {
            return $this->redirect("");
        }

        $customer = Customer::get_by_id($id);

        if (!$customer) {
            return $this->HttpError("404");
        }

        return ['Customer' => $customer];
    }

    public function ProsesRepairPassword()
    {
        $email = isset($_POST['Email']) ? $_POST['Email'] : null;
        $password = isset($_POST['Password']) ? $_POST['Password'] : null;
        $cpassword = isset($_POST['CPassword']) ? $_POST['CPassword'] : null;

        if (!$email || !$password || !$cpassword) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Email, Password dan Confirm Password tidak boleh kosong'
            ]);
        }

        if ($password == '' || $cpassword == '') {
            return json_encode([
                'Status' => 417,
                'Message' => 'Email, Password dan Confirm Password tidak boleh kosong'
            ]);
        }

        if (strlen($password) < 8) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Panjang Password harus minimal 8 karakter'
            ]);
        }

        if ($password != $cpassword) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Password dan Confirm Password harus sama'
            ]);
        }

        $customer = Customer::get()->filter('Email', $email)->first();
        if (!$customer) {
            // Hampir tidak mungkin terjadi
            return json_encode([
                'Status' => 404,
                'Message' => 'Email tidak ditemukan'
            ]);
        }

        $customer->Password = $password;
        $customer->write();

        return json_encode([
            'Status' => 201,
            'Message' => 'Sukses Repair Password, Klik Ok lalu Anda akan diarahkan ke Login.'
        ]);
    }

    public function Login()
    {
        // if (isset($_SESSION)) {
        //     var_dump($_SESSION);die();
        // }
        $Redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : null;
        $Flash =  $this->getRequest()->getSession()->get('Flash');
        if ($Flash != null) {
            $this->getRequest()->getSession()->clear('Flash');
        }
        return ['Flash' => $Flash, 'GoTo' => $Redirect];
    }

    public function ProsesLogin($data)
    {
        // $_SESSION['Tes'] = 'Halo';
        // var_dump($_SESSION);die();
        $session = $this->getRequest()->getSession();
        $email = $_POST['Email'];
        $password = $_POST['Password'];
        if (!$email || !$password) {
            $session->set('Flash', 'Mohon isi Email dan Password');
            return $this->RedirectBack();
        }

        $result = ValidationResult::create();
        $auth = new MemberAuthenticator();
        $user = $auth->authenticate($_REQUEST, $data);

        if (!$user) {
            $session->set('Flash', 'Email/Password Salah');
            return $this->RedirectBack();
        }

        if ($user->ClassName == "Kurir") {
            $session->set('Flash', 'Anda tidak bisa Login disini! Coba Login as Kurir');
            return $this->RedirectBack();
        }

        // var_dump($user);die();
        // $user->logIn(true);

        $identityStore = Injector::inst()->get(IdentityStore::class);
        $identityStore->logIn($user);

        if (isset($_POST['GoTo'])) {
            if ($_POST['GoTo'] == 'order') {
                return $this->redirect('order');
            }
        }

        return $this->redirect(CustomerPage::get()->first()->Link() . 'Dashboard');
    }

    public function EditProfile()
    {
        $kota = Kota::get();
        $kecamatan = Kecamatan::get();
        $kodepos = KodePos::get();
        return ['Kota' => $kota, 'Kecamatan' => $kecamatan, 'KodePos' => $kodepos, 'BC' => 'EditProfile'];
    }

    public function UploadPhoto()
    {
        $file = isset($_FILES['Photo']) ? $_FILES['Photo'] : null;
        if (!$file) {
            return $this->redirect('');
        }

        $member = Member::currentUser();
        if ($member) {
            $upload = new Upload();
            $image = Image::create();

            if (!$upload->loadIntoFile($file, $image, 'PhotoProfile')) {
                $this->getRequest()->getSession()->set('Flash', 'Failed Upload Image');
                return $this->redirect(CustomerPage::get()->first()->Link() . 'editprofile');
            }

            $member->Image = $image;
            $member->write();

            return $this->redirect(CustomerPage::get()->first()->Link());
        }
        return $this->redirect(CustomerPage::get()->first()->Link() . 'login');
    }

    public function ProsesEditProfile()
    {
        $member = Member::currentUser();
        if ($member) {
            $firstname = $_POST['FirstName'] != '' ? $_POST['FirstName'] : $member->FirstName;
            $surname = $_POST['Surname'] != '' ? $_POST['Surname'] : $member->Surname;
            $email = $_POST['Email'] != '' ? $_POST['Email'] : $member->Email;
            $phone = $_POST['NoTelp'] != '' ? $_POST['NoTelp'] : $member->NoTelp;
            $kotaid = $_POST['KotaID'] != '' ? $_POST['KotaID'] : $member->KotaID;
            $kecamatanid = $_POST['KecamatanID'] != '' ? $_POST['KecamatanID'] : $member->KecamatanID;
            $kodepos = $_POST['KodePos'] != '' ? $_POST['KodePos'] : $member->KodePosID;
            $address = $_POST['Address'] != '' ? $_POST['Address'] : $member->Address;

            $member->FirstName = $firstname;
            $member->Surname = $surname;
            $member->Email = $email;
            $member->NoTelp = $phone;
            $member->KotaID = $kotaid;
            $member->KecamatanID = $kecamatanid;
            $member->KodePos = $kodepos;
            $member->Address = $address;
            $member->write();

            return $this->redirect(CustomerPage::get()->first()->Link() . 'Dashboard');
        }
        return $this->redirect('');
    }

    public function Logout()
    {
        Injector::inst()->get(IdentityStore::class)->logOut();

        return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
    }

    public function PendingOrder()
    {
        $customer = Member::currentUser();
        if (!$customer) {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }

        $Error = $this->getRequest()->getSession()->get('Error');
        if ($Error) {
            $this->getRequest()->getSession()->clear('Error');
        }

        $Sukses = $this->getRequest()->getSession()->get('Sukses');
        if ($Sukses) {
            $this->getRequest()->getSession()->clear('Sukses');
        }

        $order = Order::get()->filter('CustomerID', $customer->ID)->filter('StatusPengiriman:LessThan', 2)->sort('LastEdited', 'DESC');

        $order = PaginatedList::create(
            $order,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('p');

        return ['Order' => $order, 'Voucher' => Voucher::get()->filter('Expires:GreaterThan', date('Y-m-d')), 'Sukses' => $Sukses, 'Error' => $Error, 'BC' => 'PendingOrder'];
    }

    public function Bayar()
    {
        // var_dump($_REQUEST);die();
        $session = $this->getRequest()->getSession();
        $photo = isset($_FILES['Photo']) ? $_FILES['Photo'] : null;
        $state = isset($_POST['state']) ? $_POST['state'] : null;
        if (!$photo || $photo['name'] == '') {
            $session->set('Error', 'Mohon isi Foto Bukti Pembayaran');
            return $this->RedirectBack();
        }

        $extension = $photo['type'];
        $extension = substr($extension, strpos($extension, "/")+1);
        if ($extension != 'png' && $extension != 'jpg' && $extension != 'jpeg') {
            $session->set('Error', 'Extension not allowed');
            return $this->RedirectBack();
        }
        $nomorresi = $_POST['NomorResi'];
        $order = Order::get()->filter('NomorResi', $nomorresi)->first();
        $member = Member::currentUser();
        if ($member) {
            $upload = new Upload();
            $image = Image::create();

            if (!$upload->loadIntoFile($photo, $image, 'BuktiPembayaran')) {
                $this->getRequest()->getSession()->set('Error', 'Failed Upload Image');
                return $this->redirect(CustomerPage::get()->first()->Link() . 'pendingorder');
            }

            $cost = $order->TotalHarga;

            if ($state) {
                $voucher = Voucher::get_by_id($state);
                if ($voucher->Expires < date('Y-m-d')) {
                    $this->getRequest()->getSession()->set('Error', 'Voucher already Expired');
                    return $this->redirect(CustomerPage::get()->first()->Link() . 'pendingorder');
                }

                if ($voucher->Jenis == 'Rupiah') {
                    $cost -= $voucher->Potongan;
                }
                else {
                    $cost -= ($cost * $voucher->Potongan / 100);
                }
            }

            $order->BuktiPembayaran = $image;
            $order->StatusPembayaran = 2;
            $order->VoucherID = $state;
            $order->TotalHarga = $cost;
            $order->write();


            $this->getRequest()->getSession()->set('Sukses', 'Sukses Upload Bukti Pembayaran');
            return $this->redirect(CustomerPage::get()->first()->Link() . 'PendingOrder');
        }
        return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
    }

    public function CancelOrder()
    {
        $nomorresi = isset($_GET['NomorResi']) ? $_GET['NomorResi'] : null;
        if (!$nomorresi) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Missing nomor resi'
            ]);
        }

        $member = Member::currentUser();
        if (!$member) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Anda harus login'
            ]);
        }

        $order = Order::get()->filter('NomorResi', $nomorresi)->first();
        if (!$order) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Order tidak ditemukan'
            ]);
        }

        if ($order->CustomerID != $member->ID) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Order ini bukan milik Anda'
            ]);
        }

        $order->StatusPengiriman = 5;
        $order->write();

        return json_encode([
            'Status' => 200,
            'Message' => 'Sukses'
        ]);
    }

    public function OnGoingOrder()
    {
        $customer = Member::currentUser();
        if (!$customer) {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }

        $order = Order::get()->filter(['CustomerID' => $customer->ID, 'StatusPengiriman' => 2])->sort('LastEdited', 'DESC');

        $order = PaginatedList::create(
            $order,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('p');

        return ['Order' => $order, 'BC' => 'OnGoingOrder'];
    }

    public function HistoryOrder()
    {
        $customer = Member::currentUser();
        if (!$customer) {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }

        $session = $this->getRequest()->getSession();
        $Sukses = $session->get("Sukses");
        if ($Sukses) {
            $session->clear("Sukses");
        }
        $Error = $session->get("Error");
        if ($Error) {
            $session->clear("Error");
        }

        $order = Order::get()->filter(['CustomerID' => $customer->ID, 'StatusPengiriman:GreaterThan' => 2])->sort('LastEdited', 'DESC');
        $val = 0;
        if (isset($_GET['Date']) && $_GET['Date'] != '') {
            $val = $_GET['Date'];
            $temp = ArrayList::create();
            foreach ($order as $key) {
                $year = date('Y', strtotime($key->Created));
                $month = date('m', strtotime($key->Created));

                if ($val == $year . $month) {
                    $temp->push($key);
                }
            }
            $order = $temp;
        }

        $order = PaginatedList::create(
            $order,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('p');

        $order_all = Order::get()->filter(['CustomerID' => $customer->ID, 'StatusPengiriman' => 3]);
        $filter = ArrayList::create();
        foreach ($order_all as $key) {
            $year = date('Y', strtotime($key->Created));
            $month = date('m', strtotime($key->Created));
            $ada = false;
            foreach ($filter as $value) {
                if ($year == $value->Year && $month == $value->Month) {
                    $ada = true;
                }
            }
            if (!$ada) {
                $object = DataObject::create();
                $object->Value = $year . $month;
                $object->Year = $year;
                $object->Month = $month;
                $filter->push($object);
            }
        }

        // $order = Order::get()->filter(['CustomerID' => $customer->ID, 'StatusPengiriman' => 3]);
        return ['Order' => $order, 'DateFilter' => $filter, 'Value' => $val, 'Sukses' => $Sukses, 'Error' => $Error, 'BC' => 'HistoryOrder'];
    }

    public function Rate()
    {
        $nomorresi = isset($_POST['NomorResi']) ? $_POST['NomorResi'] : null;
        $rate = isset($_POST['Rate']) ? $_POST['Rate'] : null;
        $message = isset($_POST['Message']) ? $_POST['Message'] : '';

        if (!$nomorresi && !$rate) {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'HistoryOrder');
        }

        $order = Order::get()->filter('NomorResi', $nomorresi)->first();
        $order->Rate = $rate;
        $order->RateMessage = $message;
        $order->write();

        $this->getRequest()->getSession()->set('Sukses', 'Sukses Rate Kurir');
        return $this->redirect(CustomerPage::get()->first()->Link() . 'HistoryOrder');
    }

    public function GetVoucher()
    {
        $id = $this->getRequest()->param('ID');

        $voucher = Voucher::get_by_id($id);
        if (!$voucher) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Voucher tidak ditemukan'
            ]);
        }

        return json_encode([
            'Status' => 200,
            'Message' => 'Sukses Get Voucher',
            'Voucher' => $voucher->toArray()
        ]);
    }
}
