<?php

use SilverStripe\Forms\ListboxField;
use SilverStripe\Control\Director;
use SilverStripe\Forms\HTMLEditor\HtmlEditorField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Post extends DataObject
{
    /**
     * Default sort ordering
     * @var array
     */
    private static $default_sort = ['ID' => 'DESC'];

    private static $db = [
        'Title' => 'Varchar(150)',
        'Description' => 'HTMLText',
        'URLSegment' => 'Varchar(200)'
    ];

    private static $has_one = [
        'Image' => Image::class,
        'Category' => PostCategory::class
    ];

    private static $many_many = [
        'Tags' => PostTag::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'Title',
        'Category.Title' => 'Category'
    ];

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function onBeforeWrite()
    {
        $urlSegment = $this->clean($this->Title);
        $count = count(Post::get()->filter(['URLSegment:PartialMatch'=>$urlSegment]));
        if ($count > 1) {
            $urlSegment .= '-' . ($count+1);
        }
        $this->URLSegment = $urlSegment;

        if ($this->Image->exists() && !$this->Image->isPublished()) {
            $this->Image->doPublish();
        }
	    parent::onBeforeWrite();
    }

    public function getCMSFields()
    {
        $fields = FieldList::create(
            UploadField::create(
                'Image',
                'Image'
            ),
            TextField::create(
                'Title',
                'Title'
            ),
            DropdownField::create(
                'CategoryID',
                'Category',
                PostCategory::get()->map('ID', 'Title')
            ),
            ListboxField::create('Tags', 'Tag')->setSource(PostTag::get()->sort('ID')->map('ID', 'Title'))->setAttribute('data-placeholder', 'Select tags'),
            HtmlEditorField::create(
                'Description',
                'Description'
            )
        );

        return $fields;
    }

    public function getLink()
    {
        $page = BlogPage::get()->first();
        if ($page) {
            return $page->AbsoluteLink() . 'Detail/' . $this->URLSegment;
        }
        return '';
    }

    // public function Date()
    // {
    //     // var_dump($this->Created->Month() . ' ' . $this->Created->DayOfMonth() . ', ' . $this->Created->Year());die();
    //     return $this->Created->Month() . ' ' . $this->Created->DayOfMonth() . ', ' . $this->Created->Year();
    // }

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['Created'] = date('j F Y', strtotime($this->Created));
        $arr['Title'] = $this->Title;
        $arr['Description'] = $this->Description;
        $arr['ImageURL'] = $this->Image()->AbsoluteURL;
        $arr['Category'] = $this->Category()->Title;
        return $arr;
    }
}
