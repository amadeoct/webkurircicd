<?php

use SilverStripe\Control\Director;
use SilverStripe\Assets\Image;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\Member;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Assets\Upload;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class CustomerApiPage extends ApiPage
{
    static function EmailTokenSend($customer)
    {
        $config = SiteConfig::current_site_config();
        $url = Director::absoluteBaseURL();
        $from = 'no-reply@webkurir.com';
        $to = $customer->Email;
        $link = CustomerPage::get()->first()->AbsoluteLink() . 'VerifyEmail/' . CustomToken::Generate($customer);
        $subject = $config->Title . ' - Email Verification';
        if (substr($url, 0, 16) != 'http://localhost') {
            // SMTP Configuration
            $transport = (new Swift_SmtpTransport($config->SMTPHost, $config->SMTPPort,'tls'))
            ->setUsername($config->SMTPUsername)
            ->setPassword($config->SMTPPassword);
            $mailer = new Swift_Mailer($transport);
            $message = (new Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                '<html>' .
                ' <body  style="background-color: #5db96b">' .
                '   <div class="container" style="background-color: white; padding: 50px;">' .
                '     <h1><img src="' . $config->Logo->AbsoluteURL .'" width="150px" height="150px"> ' . $config->Title . '</h1>' .
                '     <h4 style="color: red">Email Confirmation</h4>' .
                '     <hr>' .
                '     <h3>Halo,</h3>' .
                '     Terima Kasih telah melakukan registrasi di ' . $config->Title . '<br>' .
                '     Harap Klik Tombol dibawah untuk melanjutkan Registrasi '.
                '     <br>' .
                '     <a href="' . $link . '">' .
                '       <button class="btn" style="border-radius: 50px; background-color: #5db96b">Click Here</button>' .
                '     </a><br>' .
                '     <sub>' .
                '       *If you didnt recently attempt to create an account with this email address, you can safely ignore this email.' .
                '     </sub>' .
                '     <br>' .
                '     <br>' .
                '       Best Regards,<br>' .
                '     <br>' .
                '     <br>' .
                '       '. $config->Title .'.' .
                '   </div>' .
                ' </body>' .
                '</html>',
                'text/html' // Mark the content-type as HTML
            );
            $result = $mailer->send($message);
        }
        else {
            $email = Email::create()
            ->setData([
                'Link'=> $link,
            ])
            ->setHTMLTemplate('Email\\CustomerRegistration')
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);
            $email->send();
        }
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class CustomerApiPageController extends ApiPageController
{
    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'Register',
        'ResendEmailToken',
        'Login',
        'Profile',
        'EditProfile',
        'SetPassword',
        'UploadProfilePicture',
        'HitungOngkir',
        'Order',
        'ListOrder',
        'DetailOrder',
        'UploadTransferProof',
        'TrackOrder',
        'TopUp',
        'CheckKecamatanKota'
    ];

    public function doInit()
    {
        parent::doInit();
    }

    public function Register()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $email = isset($_POST['Email']) ? $_POST['Email'] : '';
        if ($email == '') {
            return self::showMessage(400, 'Mohon isi email Anda');
        }

        $member = Member::get()->filter('Email', $email)->first();
        if ($member) {
            return self::showMessage(400, 'Email sudah terdaftar, mohon untuk melakukan login');
        }

        $member = Customer::create();
        $member->Email = $email;
        $member->Password = 'abcd1234';
        $member->write();

        CustomerApiPage::EmailTokenSend($member);

        return self::showMessage(201, 'We\'ve sent you an activation email', [
            'Member' => $member->toArray()
        ]);
    }

    public function ResendEmailToken()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $email = isset($_REQUEST['Email']) ? $_REQUEST['Email'] : '';
        if ($email == '') {
            return self::showMessage(400, 'Email is required');
        }

        $member = Customer::get()->filter('Email', $email)->first();
        if (!$member) {
            return self::showMessage(404, 'Email not found');
        }

        CustomerApiPage::EmailTokenSend($member);

        return self::showMessage(200, 'We\'ve sent you an activation email');
    }

    public function Login()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $isGoogle = isset($_POST['isGoogle']) ? $_POST['isGoogle'] : 0;
        $email = isset($_POST['Email']) ? $_POST['Email'] : '';
        $name = isset($_POST['Name']) ? $_POST['Name'] : '';
        $imageUrl = isset($_POST['ImageURL']) ? $_POST['ImageURL'] : '';
        $password = isset($_POST['Password']) ? $_POST['Password'] : '';
        $deviceid = isset($_POST['DeviceID']) ? $_POST['DeviceID'] : '';

        if ($deviceid == '') {
            return self::showMessage(400, 'Mohon isi deviceid');
        }

        if ($email == '') {
            return self::showMessage(400, 'Mohon isi email Anda');
        }

        $member = Member::get()->filter('Email', $email)->first();

        if ($isGoogle == 0) {
            if (!$member) {
                return self::showMessage(404, 'Email tidak ditemukan');
            }

            $auth = new MemberAuthenticator();
            $result = $auth->checkPassword($member, $password);
            if (!$result->isValid()) {
                return self::showMessage(400, 'Password salah');
            }
        }
        else {
            if (!$member) {
                $member = Customer::create();
                $name = explode(' ', $name);
                $firstName = $name[0];
                $surname = '';
                for ($i=1; $i < count($name); $i++) {
                    if ($i != 1) {
                        $surname .= ' ';
                    }
                    $surname .= $name[$i];
                }
                $member->Email = $email;
                $member->FirstName = $firstName;
                $member->Surname = $surname;
                if ($imageUrl != '') {
                    $member->initImage = $imageUrl;
                }
                $member->write();
            }
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, 'Kurir tidak boleh login pada aplikasi ini');
        }

        $token = AccessToken::create();
        $token->MemberID = $member->ID;
        $token->DeviceID = $deviceid;
        $token->write();

        return self::showMessage(201, 'Login Sukses', [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function Profile()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        return self::showMessage(200, 'Sukses Get Member Profile', [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function EditProfile()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $member->FirstName = isset($_POST['FirstName']) ? $_POST['FirstName'] : $member->FirstName;
        $member->Surname = isset($_POST['Surname']) ? $_POST['Surname'] : $member->Surname;
        $member->NoTelp = isset($_POST['Phone']) ? $_POST['Phone'] : $member->NoTelp;
        $member->KodePos = isset($_POST['PostCode']) ? $_POST['PostCode'] : $member->KodePos;

        $city = isset($_POST['City']) ? $_POST['City'] : '';
        if ($city != '') {
            $city = explode(', ', $city);
            $district = $city[1];
            $city = $city[0];

            $district = Kecamatan::get()->filter('Title', $district)->first();
            $city = Kota::get()->filter('Title', $city)->first();

            $member->KotaID = $city->ID;
            $member->KecamatanID = $district->ID;
        }

        $member->write();

        return self::showMessage(201, 'Sukses Edit Profile', [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function SetPassword()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $password = isset($_POST['Password']) ? $_POST['Password'] : '';
        $confirm_password = isset($_POST['CPassword']) ? $_POST['CPassword'] : '';

        if ($password == '' || $confirm_password == '') {
            return self::showMessage(400, 'Passwrod and Confirmation Password is required');
        }

        if ($password != $confirm_password) {
            return self::showMessage(400, 'Password should match with Confirmation Password');
        }

        try {
            $member->Password = $password;
            $member->write();
        } catch (\Throwable $th) {
            return self::showMessage(400, $th->getMessage());
        }

        return self::showMessage(201, "Success set password", [
            'Member' => $member->toArray()
        ]);
    }

    public function UploadProfilePicture()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $image = isset($_FILES['Image']) ? $_FILES['Image'] : null;
        if (!$image) {
            return self::showMessage(400, "Mohon untuk memasukkan image");
        }

        $upload = new Upload();
        $imageobject = Image::create();

        $extension = substr($image['name'], strlen($image['name']) - 4);
        if ($extension != '.jpg' && $extension != 'jpeg' && $extension != '.png') {
            return self::showMessage(400, "Image extension not allowed");
        }

        try {
            $upload->loadIntoFile($image, $imageobject, 'CustomerProfilePictures');
        } catch (\Exception $th) {
            return self::showMessage(500, "Gagal upload image");
        }

        $member->Image = $imageobject;
        $member->write();

        return self::showMessage(201, "Sukses upload image", [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function HitungOngkir()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $from = isset($_REQUEST['From']) ? $_REQUEST['From'] : '';
        $to = isset($_REQUEST['To']) ? $_REQUEST['To'] : '';

        if ($from == "" || $to == "") {
            return self::showMessage(400, "Mohon isi asal dan tujuan");
        }

        $from = explode(',', str_replace(' ', '', $from));
        $to = explode(',', str_replace(' ', '', $to));

        // var_dump($to[1]);die();

        $kecamatanFrom = Kecamatan::get()->filter('Title:nocase', $from[1]);
        if (count($kecamatanFrom) > 1) {
            foreach ($kecamatanFrom as $key => $value) {
                if (strtolower($value->Kota()->Title) == strtolower($from[0])) {
                    $kecamatanFrom = $value;
                }
            }
        }
        else {
            $kecamatanFrom = $kecamatanFrom->first();
        }
        // var_dump($kecamatanFrom);die();

        $kecamatanTo = Kecamatan::get()->filter('Title:nocase', $to[1]);
        if (count($kecamatanTo) > 1) {
            foreach ($kecamatanTo as $key => $value) {
                if (strtolower($value->Kota()->Title) == strtolower($to[0])) {
                    $kecamatanTo = $value;
                }
            }
        }
        else {
            $kecamatanTo = $kecamatanTo->first();
        }

        $size = isset($_REQUEST['Size']) ? $_REQUEST['Size'] : 0;
        $type = isset($_REQUEST['Type']) ? $_REQUEST['Type'] : 0;

        $ongkir = HargaOngkir::get()->filter([
            'DariKotaID' => $kecamatanFrom->KotaID,
            'DariKotaBagianID' => $kecamatanFrom->KotaBagianID,
            'KeKotaID' => $kecamatanTo->KotaID,
            'KeKotaBagianID' => $kecamatanTo->KotaBagianID,
            'Size' => $size,
            'Type' => $type
        ])->first();
        while (!$ongkir && $size > 0) {
            $size--;
            $ongkir = HargaOngkir::get()->filter([
                'DariKotaID' => $kecamatanFrom->KotaID,
                'DariKotaBagianID' => $kecamatanFrom->KotaBagianID,
                'KeKotaID' => $kecamatanTo->KotaID,
                'KeKotaBagianID' => $kecamatanTo->KotaBagianID,
                'Size' => $size,
                'Type' => $type
            ])->first();
        }

        $harga = 0;
        if ($ongkir) {
            $harga = $ongkir->Harga;
        }

        return self::showMessage(200, "Sukses get ongkir",[
            "Harga" => $harga,
            'DariKotaID' => $kecamatanFrom->KotaID,
            'DariKotaBagianID' => $kecamatanFrom->KotaBagianID,
            'KeKotaID' => $kecamatanTo->KotaID,
            'KeKotaBagianID' => $kecamatanTo->KotaBagianID,
            'Size' => $size,
            'Type' => $type
        ]);
    }

    public function Order()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $sendername = isset($_REQUEST['SenderName']) ? $_REQUEST['SenderName'] : '';
        $senderemail = isset($_REQUEST['SenderEmail']) ? $_REQUEST['SenderEmail'] : '';
        $senderphone = isset($_REQUEST['SenderPhone']) ? $_REQUEST['SenderPhone'] : '';
        // $senderaddress = isset($_REQUEST['SenderAddress']) ? $_REQUEST['SenderAddress'] : '';
        $sendercity = isset($_REQUEST['SenderCity']) ? $_REQUEST['SenderCity'] : '';
        $senderdistrict = isset($_REQUEST['SenderDistrict']) ? $_REQUEST['SenderDistrict'] : '';
        $senderaddressdetail = isset($_REQUEST['SenderAddressDetail']) ? $_REQUEST['SenderAddressDetail'] : '';
        $senderlat = isset($_REQUEST['SenderLat']) ? $_REQUEST['SenderLat'] : 0.0;
        $senderlong = isset($_REQUEST['SenderLong']) ? $_REQUEST['SenderLong'] : 0.0;
        $sendernote = isset($_REQUEST['SenderNote']) ? $_REQUEST['SenderNote'] : '';

        if ($sendername == '' || $senderphone == '' || $sendercity == '' || $senderdistrict == '' || $senderlat == 0.0 || $senderlong == 0.0) {
            return self::showMessage(400, 'Mohon isi data pengirim');
        }

        $receivername = isset($_REQUEST['ReceiverName']) ? $_REQUEST['ReceiverName'] : '';
        $receiveremail = isset($_REQUEST['ReceiverEmail']) ? $_REQUEST['ReceiverEmail'] : '';
        $receiverphone = isset($_REQUEST['ReceiverPhone']) ? $_REQUEST['ReceiverPhone'] : '';
        // $receiveraddress = isset($_REQUEST['ReceiverAddress']) ? $_REQUEST['ReceiverAddress'] : '';
        $receivercity = isset($_REQUEST['ReceiverCity']) ? $_REQUEST['ReceiverCity'] : '';
        $receiverdistrict = isset($_REQUEST['ReceiverDistrict']) ? $_REQUEST['ReceiverDistrict'] : '';
        $receiveraddressdetail = isset($_REQUEST['ReceiverAddressDetail']) ? $_REQUEST['ReceiverAddressDetail'] : '';
        $receiverlat = isset($_REQUEST['ReceiverLat']) ? $_REQUEST['ReceiverLat'] : '';
        $receiverlong = isset($_REQUEST['ReceiverLong']) ? $_REQUEST['ReceiverLong'] : '';
        $receivernote = isset($_REQUEST['ReceiverNote']) ? $_REQUEST['ReceiverNote'] : '';

        if ($receivername == '' || $receiverphone == '' || $receivercity == '' || $receiverdistrict == '' || $receiverlat == 0.0 || $receiverlong == 0.0) {
            return self::showMessage(400, 'Mohon isi data penerima');
        }

        $weight = isset($_POST['Weight']) ? $_POST['Weight'] : '';
        $volume = isset($_POST['Volume']) ? $_POST['Volume'] : '';
        $preferredtime = isset($_POST['PickupTime']) ? $_POST['PickupTime'] : date("h:i:s");
        $packagedetail = isset($_POST['PackageDetail']) ? $_POST['PackageDetail'] : '';
        $fragile = isset($_POST['Fragile']) ? $_POST['Fragile'] : 0;
        $cost = isset($_POST['Cost']) ? $_POST['Cost'] : '';
        $type = isset($_POST['Type']) ? $_POST['Type'] : 0;
        $paymentMethod = isset($_POST['PaymentMethod']) ? $_POST['PaymentMethod'] : 0;

        if ($weight == '' || $volume== '' || $packagedetail == '' || $cost == '') {
            return self::showMessage(400, 'Mohon isi data pengiriman');
        }

        // $senderdistrict = explode(',', str_replace(' ', '', $senderaddress));
        $sendercityid = Kota::get()->filter('Title', $sendercity)->first()->ID;
        $senderdistrictid = Kecamatan::get()->filter([
            'Title' => $senderdistrict,
            'KotaID' => $sendercityid
        ])->first()->ID;

        // $receiverdistrict = explode(',', str_replace(' ', '', $receiveraddress));
        $receivercityid = Kota::get()->filter('Title', $receivercity)->first()->ID;
        $receiverdistrictid = Kecamatan::get()->filter([
            'Title' => $receiverdistrict,
            'KotaID' => $receivercityid
        ])->first()->ID;

        $order = Order::create();
        // Sender Data
        $order->PengirimNama = $sendername;
        $order->PengirimEmail = $senderemail;
        $order->PengirimNoTelp = $senderphone;
        $order->PengirimKotaID = $sendercityid;
        $order->PengirimKecamatanID = $senderdistrictid;
        $order->PengirimAlamat = $senderaddressdetail;
        $order->PengirimLat = $senderlat;
        $order->PengirimLong = $senderlong;
        $order->PengirimNote = $sendernote;
        // Receiver Data
        $order->PenerimaNama = $receivername;
        $order->PenerimaEmail = $receiveremail;
        $order->PenerimaNoTelp = $receiverphone;
        $order->PenerimaKotaID = $receivercityid;
        $order->PenerimaKecamatanID = $receiverdistrictid;
        $order->PenerimaAlamat = $receiveraddressdetail;
        $order->PenerimaLat = $receiverlat;
        $order->PenerimaLong = $receiverlong;
        $order->PenerimaNote = $receivernote;
        // Item Data
        $order->BarangJenis = $packagedetail;
        $order->BarangBerat = $weight;
        $order->BarangVolume = $volume;
        $order->JamPengambilan = $preferredtime;
        $order->TotalHarga = $cost;
        $order->CustomerID = $member->ID;
        $order->IsFragile = $fragile;
        $order->Type = $type;
        $order->VoucherID = isset($_REQUEST['VoucherID']) ? $_REQUEST['VoucherID'] : 0;

        if ($paymentMethod == 0) {
            $order->PaymentMethod = 'Manual Transfer';
        }
        else {
            $order->PaymentMethod = 'Wallet';
            $order->StatusPembayaran = 3;

            $member->Wallet -= $cost;
            $member->write();
        }

        // var_dump($order);die();
        $order->write();

        return self::showMessage(201, "Sukses Order Pengiriman",[
            'Order' => $order->toArray()
        ]);
    }

    public function ListOrder()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $SortBy = isset($_REQUEST['SortBy']) ? $_REQUEST['SortBy'] : 'ID';
        $SortType = isset($_REQUEST['SortType']) ? $_REQUEST['SortType'] : 'DESC';
        $Limit = isset($_REQUEST['Limit']) ? $_REQUEST['Limit'] : 10;
        $Offset = isset($_REQUEST['Offset']) ? $_REQUEST['Offset'] : 0;

        $order = Order::get()->filter('CustomerID', $member->ID)->sort($SortBy, $SortType)->limit($Limit, $Offset);
        $arrOrder = [];
        foreach ($order as $key => $value) {
            $arrOrder[] = $value->toArray();
        }

        return self::showMessage(200, 'Sukses get list order', [
            'Order' => $arrOrder
        ]);
    }

    public function DetailOrder()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $id = $this->getRequest()->param('ID');
        if (!$id) {
            return self::showMessage(400, 'ID is Required');
        }

        $order = Order::get_by_id($id);
        if ($order->CustomerID != $member->ID) {
            return self::showMessage(400, 'This is not your ourder');
        }

        return self::showMessage(200, 'Sukses get order detail', [
            'Order' => $order->toArray()
        ]);
    }

    public function UploadTransferProof()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $id = $this->getRequest()->param('ID');
        if (!$id) {
            return self::showMessage(400, "ID Order Required");
        }

        $order = Order::get_by_id($id);
        if (!$order) {
            return self::showMessage(400, "Order not found");
        }

        $image = isset($_FILES['Image']) ? $_FILES['Image'] : null;
        if (!$image) {
            return self::showMessage(400, "Mohon untuk memasukkan image");
        }

        $upload = new Upload();
        $imageobject = Image::create();

        $extension = substr($image['name'], strlen($image['name']) - 4);
        if ($extension != '.jpg' && $extension != 'jpeg' && $extension != '.png') {
            return self::showMessage(400, "Image extension not allowed");
        }

        try {
            $upload->loadIntoFile($image, $imageobject, 'TransferProofs');
        } catch (\Exception $th) {
            return self::showMessage(500, "Gagal upload image");
        }

        $order->PaymentMethod = "Manual Transfer";
        $order->BuktiPembayaran = $imageobject;
        $order->StatusPembayaran = 2;
        $order->write();

        $track = OrderTrack::create();
        $track->OrderID = $order->ID;
        $track->Title = "Payment received using " . $order->PaymentMethod;
        $track->Timestamp = date('Y-m-d H:i:s');
        $track->write();

        return self::showMessage(201, "Sukses upload image", [
            'Order' => $order->toArray()
        ]);
    }

    public function TrackOrder()
    {
        $id = $this->getRequest()->param('ID');
        if (!$id) {
            return self::showMessage(400, 'ID Order is Required');
        }

        $order = Order::get_by_id($id);
        if (!$order) {
            return self::showMessage(400, 'Order not found');
        }

        $arr_track = [];
        foreach ($order->Track()->sort('ID', 'DESC') as $key => $value) {
            $arr_track[] = $value->toArray();
        }

        $arr_order = $order->toArray();
        $arr_order['Track'] = $arr_track;

        return self::showMessage(200, 'Sukses Tracking Order', [
            'Order' => $arr_order
        ]);
    }

    public function TopUp()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $amount = $_POST['Amount'] ? $_POST['Amount'] : 0;
        if ($amount == 0) {
            return self::showMessage(400, "Amount is required");
        }

        $image = isset($_FILES['Image']) ? $_FILES['Image'] : null;
        if (!$image) {
            return self::showMessage(400, "Mohon untuk memasukkan image");
        }

        $upload = new Upload();
        $imageobject = Image::create();

        $extension = substr($image['name'], strlen($image['name']) - 4);
        if ($extension != '.jpg' && $extension != 'jpeg' && $extension != '.png') {
            return self::showMessage(400, "Image extension not allowed");
        }

        try {
            $upload->loadIntoFile($image, $imageobject, 'TransferProofs');
        } catch (\Exception $th) {
            return self::showMessage(500, "Gagal upload image");
        }

        $topuprequest = TopUpRequest::create();
        $topuprequest->Amount = $amount;
        $topuprequest->ImageID = $imageobject->ID;
        $topuprequest->CustomerID = $member->ID;
        $topuprequest->write();

        return self::showMessage(201, "Sukses Request Top Up");
    }

    public function CheckKecamatanKota()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Customer");
        }

        $city = isset($_REQUEST['City']) ? $_REQUEST['City'] : '';
        $district = isset($_REQUEST['District']) ? $_REQUEST['District'] : '';

        $kota = Kota::get()->filter([
            'Title:nocase' => $city
        ]);

        if (count($kota) <= 0) {
            return self::showMessage(404, "Kota not found");
        }

        foreach ($kota as $key => $value) {
            $kecamatan = Kecamatan::get()->filter([
                'Title:nocase' => $district,
                'KotaID' => $value->ID
            ])->first();
            if (!$kecamatan) {
                return self::showMessage(404, "Kecamatan not found");
            }
        }

        return self::showMessage(200, "All ok");
    }
}
