<style>
    .small-break {
        margin-top: 5px;
    }
</style>
<div class="theme-page">
    <% include BreadCrumbs %>
    <div class="clearfix">
        <div class="row page-margin-top-section">
            <% include CustomerSideBar %>
            <!-- Rate -->
            <div class="column column-3-4">
                <!-- Upload Foto -->
                <div id="modal" data-iziModal-title="Rate Our Kurir"> <!-- data-iziModal-fullscreen="true"  data-iziModal-title="Welcome"  data-iziModal-subtitle="Subtitle"  data-iziModal-icon="icon-home" -->
                    <!-- Modal content -->
                    <div class="container" style="padding: 10px">
                        <form action="{$Link}Rate" method="post" enctype="multipart/form-data" class="contact-form">
                            <img width="50%" id="modal-img">
                            <div class="row" style="margin-bottom: 15px; font-size: 2em">
                                Rate : <i class="far fa-star star" id="star1"></i> <i class="far fa-star star" id="star2"></i> <i class="far fa-star star" id="star3"></i> <i class="far fa-star star" id="star4"></i> <i class="far fa-star star" id="star5"></i>
                            </div>
                            <div class="row flex-box">
                                <fieldset class="column column-1-2">
                                    <label class="message">MESSAGE (Optional)</label>
                                    <textarea name="Message" class="message" id="Message"></textarea>
                                </fieldset>
                            </div>
                            <table>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="hidden" value="0" name="NomorResi" id="NomorResi">
                                            <input type="hidden" value="0" name="Rate" id="Rate">
                                            <input class="more" type="submit" value="Submit" name="submit">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <!-- Rate End -->
                <h2 class="box-header align-left">History Orders</h2>
                <% if $Order %>
                    <div style="margin-bottom: 30px">
                        <p>
                            History Order Anda yang sudah terkirim.
                        </p>
                        <select name="state" style="width: 100%" id="dropdown">
                            <option data-placeholder="true"></option>
                            <% loop $DateFilter %>
                                <option value="$Value" <% if $Value == $Up.Value %>
                                    selected
                                <% end_if %>>$Year - $Month</option>
                            <% end_loop %>
                        </select>
                        <ul class="accordion margin-top-40 clearfix">
                            <li>
                                <div id="accordion-Close" style="display: none">
                                </div>
                            </li>
                            <% loop $Order %>
                                <li>
                                    <% include OrderThumbnail %>
                                </li>
                            <% end_loop %>
                        </ul>
                        <% if $Order.MoreThanOnePage %>
                            <ul class="pagination margin-top-70">
                                <% if $Order.NotFirstPage %>
                                    <li class="left">
                                        <a href="$Order.PrevLink" title="" class="template-arrow-horizontal-3">&nbsp;</a>
                                    </li>
                                <% end_if %>
                                <% loop $Order.Pages %>
                                    <li <% if CurrentBool %> class="selected" <% end_if %>>
                                        <a href="$Link">
                                            $PageNum
                                        </a>
                                    </li>
                                <% end_loop %>
                                <% if $Order.NotLastPage %>
                                    <li class="right">
                                        <a href="$Order.NextLink" title="" class="template-arrow-horizontal-3">&nbsp;</a>
                                    </li>
                                <% end_if %>
                            </ul>
                        <% end_if %>
                    </div>
                <% else %>
                    <p>
                        Tidak ada history pengiriman.
                    </p>
                <% end_if %>
            </div>
            <!-- On Going Order End -->
        </div>
    </div>
</div>

<script>
    function CreateSlimSelect() {
        var dump = new SlimSelect({
            select: '#dropdown',
            allowDeselect: true,
            placeholder: "Filter By Date",
            afterClose: function(){
                if (dump.selected() == '') {
                    if ($Value != 0){
                        window.location.replace('{$Link}HistoryOrder')
                    }
                }
                else if (dump.selected() != '{$Value}') {
                    window.location.replace('{$Link}HistoryOrder?Date=' + dump.selected())
                }
            }
        })
        return dump;
    }
    $(document).ready(function(){
        select = CreateSlimSelect();
    });
    $('#modal').iziModal({
        title: 'Upload Foto',
        headerColor: '#56b665',
        zindex: 99999999,
    });
    $('.rate').on('click', function (event) {
        event.preventDefault();
        // $('#modal').iziModal('setZindex', 99999);
        // $('#modal').iziModal('open', { zindex: 99999 });
        $('#modal-img').attr('src', $(this).attr('imgurl'));
        var nomorresi = $(this).attr('nomorresi');
        $('#NomorResi').val(nomorresi);
        $('#modal').iziModal('open');
    });

    $('.star').on('click', function(){
        $('.star').removeClass('fas');
        $('.star').removeClass('far');
        var id = $(this).attr('id');
        id = parseInt(id.substr(4));
        $('#Rate').val(id);
        for(var i = 0 ; i < id ; i++) {
            $('#star'+(i+1)).addClass('fas');
        }
        for(var i = id ; i < 5 ; i++) {
            $('#star'+(i+1)).addClass('far');
        }
    });
</script>
