function changeValid(e) {
    var expires = jQuery("#FieldExpires").val();
    if (expires == "" || e.value > expires) {
        jQuery("#FieldExpires").val(e.value);
    }
}

function changeExpires(e) {
    var valid = jQuery("#FieldValid").val();
    if (valid == "" || e.value < valid) {
        jQuery("#FieldValid").val(e.value);
    }
}
