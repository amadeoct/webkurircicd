<?php

use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class HomePage extends Page
{
    private static $db = [
        'ReasonsDescription' => 'Text',
    ];

    private static $has_one = [
        'ReasonsImage' => Image::class
    ];

    private static $has_many = [
        'Reasons' => Reasons::class,
        'Slides' => Slides::class
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->ReasonsImage->exists() && !$this->ReasonsImage->isPublished()) {
            $this->ReasonsImage->doPublish();
        }
    }

    /**
     * CMS Fields
     * @return FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab(
            'Root.Slides',
            [
                GridField::create(
                    'Slides',
                    'Slides',
                    $this->Slides(),
                    GridFieldConfig_RecordEditor::create()
                )
            ]
        );
        $fields->addFieldsToTab(
            'Root.ReasonToChooseUsSection',
            [
                UploadField::create(
                    'ReasonsImage',
                    'Image (Potrait)'
                ),
                TextareaField::create(
                    'ReasonsDescription',
                    'Description'
                ),
                GridField::create(
                    'Reasons',
                    'Reasons',
                    $this->Reasons(),
                    GridFieldConfig_RecordEditor::create()
                )
            ]
        );
        return $fields;
    }

    public function AboutUsLink()
    {
        return AboutPage::get()->first()->Link();
    }
}
