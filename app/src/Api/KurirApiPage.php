<?php

use SilverStripe\Security\Member;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Assets\Upload;
use SilverStripe\Assets\Image;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class KurirApiPage extends ApiPage
{

}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class KurirApiPageController extends ApiPageController
{
    public function doInit()
    {
        parent::doInit();
    }

    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'Login',
        'Profile',
        'EditProfile',
        'UploadProfilePicture',
        'ListOrder',
        'DetailOrder',
        'UpdateStatusPengiriman',
        'SwitchIsGPS'
    ];

    public function Login()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $email = isset($_POST['Email']) ? $_POST['Email'] : '';
        $password = isset($_POST['Password']) ? $_POST['Password'] : '';
        $deviceid = isset($_POST['DeviceID']) ? $_POST['DeviceID'] : '';

        if ($deviceid == '') {
            return self::showMessage(400, 'Mohon isi deviceid');
        }

        if ($email == '') {
            return self::showMessage(400, 'Mohon isi email Anda');
        }

        $member = Member::get()->filter('Email', $email)->first();

        if (!$member) {
            return self::showMessage(404, 'Email tidak ditemukan');
        }

        $auth = new MemberAuthenticator();
        $result = $auth->checkPassword($member, $password);
        if (!$result->isValid()) {
            return self::showMessage(400, 'Password salah');
        }

        if ($member->ClassName == 'Customer') {
            return self::showMessage(400, 'Customer tidak boleh login pada aplikasi ini');
        }

        $token = AccessToken::create();
        $token->MemberID = $member->ID;
        $token->DeviceID = $deviceid;
        $token->write();

        return self::showMessage(201, 'Login Sukses', [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function Profile()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Customer') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Kurir");
        }

        return self::showMessage(200, 'Sukses Get Member Profile', [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function EditProfile()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName != 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Kurir");
        }

        $member->FirstName = isset($_POST['FirstName']) ? $_POST['FirstName'] : $member->FirstName;
        $member->Surname = isset($_POST['Surname']) ? $_POST['Surname'] : $member->Surname;
        $member->NoTelp = isset($_POST['Phone']) ? $_POST['Phone'] : $member->NoTelp;

        $member->write();

        return self::showMessage(201, 'Sukses Edit Profile', [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function UploadProfilePicture()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return json_encode('This request requires post submission');
        }

        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName != 'Kurir') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Kurir");
        }

        $image = isset($_FILES['Image']) ? $_FILES['Image'] : null;
        if (!$image) {
            return self::showMessage(400, "Mohon untuk memasukkan image");
        }

        $upload = new Upload();
        $imageobject = Image::create();

        $extension = substr($image['name'], strlen($image['name']) - 4);
        if ($extension != '.jpg' && $extension != 'jpeg' && $extension != '.png') {
            return self::showMessage(400, "Image extension not allowed");
        }

        try {
            $upload->loadIntoFile($image, $imageobject, 'KurirProfilePictures');
        } catch (\Exception $th) {
            return self::showMessage(500, "Gagal upload image");
        }

        $member->Image = $imageobject;
        $member->write();

        return self::showMessage(201, "Sukses upload image", [
            'AccessToken' => $token->Token,
            'Member' => $member->toArray()
        ]);
    }

    public function ListOrder()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName == 'Customer') {
            return self::showMessage(400, "Api hanya boleh digunakan oleh Kurir");
        }

        $SortBy = isset($_REQUEST['SortBy']) ? $_REQUEST['SortBy'] : 'ID';
        $SortType = isset($_REQUEST['SortType']) ? $_REQUEST['SortType'] : 'DESC';
        $Limit = isset($_REQUEST['Limit']) ? $_REQUEST['Limit'] : 10;
        $Offset = isset($_REQUEST['Offset']) ? $_REQUEST['Offset'] : 0;

        $order = Order::get()->filter('KurirID', $member->ID)->sort($SortBy, $SortType)->limit($Limit, $Offset);
        $arrOrder = [];
        foreach ($order as $key => $value) {
            $arrOrder[] = $value->toArray();
        }

        return self::showMessage(200, 'Sukses get list order', [
            'Order' => $arrOrder
        ]);
    }

    public function DetailOrder()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        $id = $this->getRequest()->param('ID');
        if (!$id) {
            return self::showMessage(400, 'ID is Required');
        }

        $order = Order::get_by_id($id);
        if ($order->KurirID != $member->ID) {
            return self::showMessage(400, 'This is not your ourder', [
                'KurirID' => $order->KurirID,
                'YourID' => $member->ID
            ]);
        }

        return self::showMessage(200, 'Sukses get order detail', [
            'Order' => $order->toArray()
        ]);
    }

    public function UpdateStatusPengiriman()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName != 'Kurir') {
            return self::showMessage(400, 'Api hanya boleh dipakai oleh kurir');
        }

        $id = $this->getRequest()->param('ID');
        if (!$id) {
            return self::showMessage(400, 'ID is Required');
        }

        $order = Order::get_by_id($id);
        if (!$order) {
            return self::showMessage(400, 'Order not found');
        }

        if ($order->KurirID != $member->ID) {
            return self::showMessage(400, 'This is not your ourder');
        }

        $status = isset($_POST['Status']) ? $_POST['Status'] : '';
        $proof = isset($_FILES['Proof']) ? $_FILES['Proof'] : null;

        if ($status == '') {
            return self::showMessage(400, 'Status is required');
        }

        $order->StatusPengiriman = $status;

        if ($status == 3 && $proof === null) {
            return self::showMessage(400, 'Status delivered requires a Proof');
        }
        else if ($status == 3) {
            $upload = new Upload();
            $proofobject = Image::create();

            $extension = substr($proof['name'], strlen($proof['name']) - 4);
            if ($extension != '.jpg' && $extension != 'jpeg' && $extension != '.png') {
                return self::showMessage(400, "Image extension not allowed");
            }

            try {
                $upload->loadIntoFile($proof, $proofobject, 'KurirDeliveryProofs');
            } catch (\Exception $th) {
                return self::showMessage(500, "Gagal upload image");
            }

            $order->BuktiPengirimanID = $proofobject->ID;
        }
        $order->write();

        if ($order->StatusPengiriman == 2) {
            $track = OrderTrack::create();
            $track->OrderID = $order->ID;
            $track->Title = "Courier has received your package";
            $track->Timestamp = date('Y-m-d H:i:s');
            $track->write();
        }
        if ($order->StatusPengiriman == 3) {
            $track = OrderTrack::create();
            $track->OrderID = $order->ID;
            $track->Title = "Your package has been delivered";
            $track->Timestamp = date('Y-m-d H:i:s');
            $track->write();
        }

        return self::showMessage(201, 'Sukses Update Status Pengiriman', [
            'Order' => $order->toArray()
        ]);
    }

    public function SwitchIsGPS()
    {
        if ($this->CheckAccessToken()) {
            return $this->CheckAccessToken();
        }

        $token = $_SERVER['HTTP_ACCESSTOKEN'];
        $token = AccessToken::get()->filter('Token', $token)->first();
        $member = Member::get_by_id($token->MemberID);
        if (!$member) {
            return self::showMessage(404, "Member not found");
        }

        if ($member->ClassName != 'Kurir') {
            return self::showMessage(400, 'Api hanya boleh dipakai oleh kurir');
        }

        if (isset($_REQUEST['Off'])) {
            $member->isGPS = 0;
        }
        else {
            switch ($member->isGPS) {
                case 0:
                    $member->isGPS = 1;
                    break;

                default:
                    $member->isGPS = 0;
                    break;
            }
        }
        $member->write();

        return self::showMessage(201, 'Sukses switch status duty');
    }
}
