<?php

use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class AboutPage extends Page
{
    private static $db = [
        'SubTitle' => 'Text',
    ];

    private static $has_one = [
        'Image1' => Image::class,
        'Image2' => Image::class,
        'Image3' => Image::class,
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->Image1->exists() && !$this->Image1->isPublished()) {
            $this->Image1->doPublish();
        }
        if ($this->Image2->exists() && !$this->Image2->isPublished()) {
            $this->Image2->doPublish();
        }
        if ($this->Image3->exists() && !$this->Image3->isPublished()) {
            $this->Image3->doPublish();
        }
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab(
            'Root.Main',
            TextareaField::create(
                'SubTitle',
                'SubTitle'
            ),
            'Content'
        );
        $fields->addFieldsToTab(
            'Root.Main',
            [
                UploadField::create(
                    'Image1',
                    'Image1 (640x480)'
                ),
                UploadField::create(
                    'Image2',
                    'Image2 (480x320)'
                ),
                UploadField::create(
                    'Image3',
                    'Image3 (480x320)'
                )
            ]
        );
        return $fields;
    }
}
