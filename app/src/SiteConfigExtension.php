<?php

use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;


class SiteConfigExtension extends DataExtension
{
    private static $db = [
        'TodaysQuote' => 'Text',

        //ContactInfo
        'City' => 'Varchar(100)',
        'Address' => 'Varchar(255)',
        'Phone' => 'Varchar(100)',
        'Email' => 'Varchar(100)',
        'EmailTo' => 'Text',
        'WorkingHours' => 'Varchar(100)',
        'WorkingDays' => 'Varchar(100)',

        //Social Media Links
        'PinterestLink' => 'Varchar(100)',
        'FacebookLink' => 'Varchar(100)',
        'TwitterLink' => 'Varchar(100)',

        //Rekening
        'AtasNama' => 'Varchar(255)',
        'NoRek' => 'Varchar(150)',
        'Bank' => 'Varchar(100)',

        // SMTP Thing
        'SMTPHost' => 'Varchar(255)',
        'SMTPUsername' => 'Varchar(255)',
        'SMTPPassword' => 'Varchar(255)',
        'SMTPPort' => 'Varchar(255)',
    ];

    private static $has_one = [
        'Logo' => Image::class,
        'LogoTransparent' => Image::class
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->owner->Logo->exists() && !$this->owner->Logo->isPublished()) {
            $this->owner->Logo->doPublish();
        }
        if ($this->owner->LogoTransparent->exists() && !$this->owner->LogoTransparent->isPublished()) {
            $this->owner->LogoTransparent->doPublish();
        }
    }

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            'Root.Rekening',
            [
                TextField::create(
                    'AtasNama',
                    'Atas Nama'
                ),
                TextField::create(
                    'NoRek',
                    'Nomor Rekening'
                ),
                TextField::create(
                    'Bank',
                    'Nama Bank'
                )
            ]
        );
        $fields->addFieldsToTab(
            'Root.SocialMedia',
            [
                TextField::create(
                    'PinterestLink',
                    'Pinterest Link'
                ),
                TextField::create(
                    'FacebookLink',
                    'Facebook Link'
                ),
                TextField::create(
                    'TwitterLink',
                    'Twitter Link'
                ),
            ]
        );
        $fields->addFieldsToTab(
            'Root.ContactInfo',
            [
                TextField::create(
                    'City',
                    'City'
                ),
                TextField::create(
                    'Address',
                    'Address'
                ),
                TextField::create(
                    'Phone',
                    'Phone'
                ),
                $email = TextField::create(
                    'Email',
                    'Email'
                ),
                $emailto = TextField::create(
                    'EmailTo',
                    'Email To'
                ),
                TextField::create(
                    'WorkingHours',
                    'WorkingHours'
                ),
                TextField::create(
                    'WorkingDays',
                    'WorkingDays'
                ),
            ]
        );
        $fields->addFieldToTab(
            'Root.Main',
            UploadField::create(
                'Logo',
                'Logo'
            )
        );
        $fields->addFieldToTab(
            'Root.Main',
            UploadField::create(
                'LogoTransparent',
                'Logo Transaparent'
            )
        );
        $fields->addFieldToTab(
            'Root.Main',
            TextareaField::create(
                'TodaysQuote',
                'Todays Quote'
            )
        );
        $fields->addFieldsToTab(
            'Root.SMTPConfiguration',
            [
                TextField::create(
                    'SMTPHost',
                    'Host'
                ),
                TextField::create(
                    'SMTPUsername',
                    'Username'
                ),
                TextField::create(
                    'SMTPPassword',
                    'Password'
                ),
                TextField::create(
                    'SMTPPort',
                    'Port'
                ),
            ]
        );

        $email->setDescription('Email yang tertampil pada website');
        $emailto->setDescription('Email yang akan menerima pesan dari SMTP (Dapat lebih dari 2, Dipisahkan dengan tanda ",")');
    }

    public function getYear()
    {
        return date('Y');
    }
}
