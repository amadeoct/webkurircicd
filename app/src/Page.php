<?php

namespace {

use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\CMS\Model\SiteTree;

    class Page extends SiteTree
    {
        private static $db = [];

        private static $has_one = [
            'Image' => Image::class
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            $fields->addFieldToTab(
                'Root.Main',
                UploadField::create(
                    'Image',
                    'Header Image'
                ),
                'Title'
            );

            return $fields;
        }

        /**
         * Event handler called before writing to the database.
         *
         * @uses DataExtension->onAfterWrite()
         */
        protected function onBeforeWrite()
        {
            parent::onBeforeWrite();
            if ($this->Image()->exists() && !$this->Image()->isPublished()) {
                $this->Image()->doPublish();
            }
        }

        public function GetVersion()
        {
            return "?v=" . date("dMYHis");
        }

        public function GetAbout()
        {
            return AboutPage::get()->first();
        }

        public function getLatestPost()
        {
            return Post::get()->sort('ID', 'DESC')->limit(3);
        }

        public function getOrderLink()
        {
            return OrderPage::get()->first()->Link();
        }

        public function getCustomerPageLink()
        {
            return CustomerPage::get()->first()->Link();
        }
    }
}
