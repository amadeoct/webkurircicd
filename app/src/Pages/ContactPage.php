<?php

use SilverStripe\Control\Director;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Email\Email;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class ContactPage extends Page
{

}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class ContactPageController extends PageController
{
    public function doInit()
    {
        parent::doInit();
    }

    public function index()
    {
        $Flash =  $this->getRequest()->getSession()->get('Flash');
        if ($Flash != null) {
            $this->getRequest()->getSession()->clear('Flash');
        }
        return ['Flash' => $Flash];
    }

    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'SendEmail'
    ];

    public function SendEmail()
    {
        // var_dump($_POST);die();
        $session = $this->getRequest()->getSession();
        $name = isset($_POST['name']) ? $_POST['name'] : null;
        $email = isset($_POST['email']) ? $_POST['email'] : null;
        $phone = isset($_POST['phone']) ? $_POST['phone'] : null;
        $message = isset($_POST['message']) ? $_POST['message'] : null;
        if (!$name || !$email || !$phone || !$message) {
            $session->set('Flash', 'Mohon isi semua kotak kosong');
            return $this->RedirectBack();
        }

        $config = SiteConfig::current_site_config();
        $url = Director::absoluteBaseURL();
        $from = 'no-reply@WebKurir.com';
        $to = explode(',', str_replace(' ', '', $config->EmailTo));
        $subject = $config->Title . ' - Message From Client['. $name .']';
        if (substr($url, 0, 16) != 'http://localhost') {
            // SMTP Configuration
            $transport = (new Swift_SmtpTransport($config->SMTPHost, $config->SMTPPort, 'tls'))
            ->setUsername($config->SMTPUsername)
            ->setPassword($config->SMTPPassword);
            $mailer = new Swift_Mailer($transport);

            $message = (new Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                '<html>' .
                ' <body  style="background-color: #5db96b">' .
                '   <div class="container" style="background-color: white; padding: 5%; margin-left: 10%; margin-right: 10%">' .
                '     <h1><img src="' . $config->Logo()->AbsoluteURL .'" width="150px" height="150px"> ' . $config->Title . '</h1>' .
                '     <h4 style="color: red">Message From Client</h4>' .
                '     <hr>' .
                '
                        <table style="border-collapse: collapse">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>'.$name.'</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>'.$email.'</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>:</td>
                                    <td>'.$phone.'</td>
                                </tr>
                                <tr>
                                    <td>Message</td>
                                    <td>:</td>
                                    <td>'.$message.'</td>
                                </tr>
                            </tbody>
                        </table>
                '.
                '     <br>' .
                '     <br>' .
                '       Best Regards,<br>' .
                '     <br>' .
                '     <br>' .
                '       '. $config->Title .'.' .
                '   </div>' .
                ' </body>' .
                '</html>',
                'text/html' // Mark the content-type as HTML
            );
            $result = $mailer->send($message);
            if ($result == 1) {
                $session->set('Flash', 'Pesan Terkirim');
            }
            else {
                $session->set('Flash', 'Error');
            }
        }
        else {
            $email = Email::create()
            ->setData([
                'Name' => $name,
                'Email' => $email,
                'Phone' => $phone,
                'Message' => $message,
                'Config' => SiteConfig::current_site_config()
            ])
            ->setHTMLTemplate('Email\\ClientMessage')
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);
            $email->send();
            $session->set('Flash', 'Pesan Terkirim');
        }
        return $this->redirect(ContactPage::get()->first()->Link());
    }
}
