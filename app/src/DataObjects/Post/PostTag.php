<?php

use SilverStripe\Control\Director;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class PostTag extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(150)',
        'URLSegment' => 'Varchar(200)'
    ];

    private static $belongs_many_many = [
        'Post' => Post::class
    ];

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        $urlSegment = $this->clean($this->Title);
        $count = count(PostTag::get()->filter(['URLSegment:PartialMatch'=>$urlSegment]));
        if ($count > 1) {
            $urlSegment .= '-' . ($count+1);
        }
        $this->URLSegment = $urlSegment;
    }

    public function getCMSFields()
    {
        return FieldList::create(
            TextField::create(
                'Title',
                'Title'
            )
        );
    }

    public function getLink()
    {
        $page = BlogPage::get()->first();
        if ($page) {
            return $page->AbsoluteLink() . '?tag=' . $this->URLSegment;
        }
        return '';
    }
}
