<?php

use SilverStripe\Security\Member;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class CustomToken extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(255)'
    ];

    private static $has_one = [
        'Member' => Member::class
    ];

    static function Generate($member)
    {
        $rand = rand(0, 999);
        $token = md5($member->Email . $rand);

        while (CustomToken::isExists($token)) {
            $rand = rand(0, 999);
            $token = md5($member->Email . $rand);
        }

        $tokenObj = CustomToken::create();
        $tokenObj->Title = $token;
        $tokenObj->MemberID = $member->ID;
        $tokenObj->write();

        return $token;
    }

    static function isExists($token)
    {
        $token = CustomToken::get()->filter('Title', $token)->first();
        if ($token) {
            return true;
        }
        return false;
    }

    public function isValid()
    {
        $date = strtotime($this->Created) + (60*60); // Token created + 1 hour (60 minutes x 60 seconds)
        if (strtotime('now') > $date) {
            return false;
        }
        return true;
    }
}
