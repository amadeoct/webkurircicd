<?php

use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Reasons extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(150)',
        'Description' => 'Text'
    ];

    private static $has_one = [
        'Home' => HomePage::class
    ];
}
