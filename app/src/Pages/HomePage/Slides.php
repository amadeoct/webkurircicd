<?php

use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Slides extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(150)',
        'Description' => 'Varchar(150)',
        'SubDescription' => 'Varchar(150)',
    ];

    private static $has_one = [
        'HomePage' => HomePage::class,
        'Image' => Image::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'Title',
        'Description',
        'SubDescription' => 'Sub Description',
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->Image()->exists() && !$this->Image()->isPublished()) {
            $this->Image()->doPublish();
        }
    }

    public function getCMSFields()
    {
        return FieldList::create(
            UploadField::create(
                'Image',
                'Image,'
            ),
            TextField::create(
                'Title',
                'Title'
            ),
            TextField::create(
                'Description',
                'Description'
            ),
            TextField::create(
                'SubDescription',
                'Sub-Description'
            )
        );
    }

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['Title'] = $this->Title;
        $arr['Description'] = $this->Description;
        $arr['ImageURL'] = $this->Image()->AbsoluteURL;
        return $arr;
    }
}
