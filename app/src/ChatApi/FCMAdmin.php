<?php

use SilverStripe\Admin\ModelAdmin;
class ProductAdmin extends ModelAdmin {
    private static $managed_models = [
        'FCMID'
    ];
    private static $menu_priority = -9999;
    private static $url_segment = 'FCM';
    private static $menu_title = 'FCM';
    
}