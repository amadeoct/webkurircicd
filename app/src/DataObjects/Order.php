<?php

use SilverStripe\View\Requirements;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\DataList;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Forms\LabelField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TimeField;
use SilverStripe\Forms\TimeField_Readonly;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Order extends DataObject
{
    private static $db = [
        'NomorResi' => 'Varchar(255)',
        'Type' => 'Int',
        'PengirimNama' => 'Varchar(150)',
        'PengirimEmail' => 'Varchar(150)',
        'PengirimNoTelp' => 'Varchar(150)',
        'PengirimAlamat' => 'Text',
        'PengirimKodePos' => 'Text',
        'PengirimLat' => 'Double',
        'PengirimLong' => 'Double',
        'PengirimNote' => 'Text',
        'PenerimaNama' => 'Varchar(150)',
        'PenerimaEmail' => 'Varchar(150)',
        'PenerimaNoTelp' => 'Varchar(150)',
        'PenerimaAlamat' => 'Text',
        'PenerimaKodePos' => 'Text',
        'PenerimaLat' => 'Double',
        'PenerimaLong' => 'Double',
        'PenerimaNote' => 'Text',
        'BarangJenis' => 'Varchar(255)',
        'BarangBerat' => 'Varchar(255)',
        'BarangVolume' => 'Varchar(255)',
        'IsFragile' => 'Int',
        'TotalHarga' => 'Int',
        'JamPengambilan' => 'Time',
        'KurirAmbil' => 'Time',
        'KurirKirim' => 'Time',
        'PembayaranAccepted' => 'Datetime',
        'StatusPengiriman' => 'Int',
        'StatusPembayaran' => 'Int',
        'Rate' => 'Int',
        'RateMessage' => 'Text',
        'PaymentMethod' => 'Text'
    ];

    private static $has_one = [
        'PengirimKota' => Kota::class,
        'PengirimKecamatan' => Kecamatan::class,
        'PenerimaKota' => Kota::class,
        'PenerimaKecamatan' => Kecamatan::class,
        'Voucher' => Voucher::class,
        'Kurir' => Kurir::class,
        'BuktiPengiriman' => Image::class,
        'BuktiPembayaran' => Image::class,
        'Customer' => Customer::class
    ];

    private static $has_many = [
        'Track' => OrderTrack::class
    ];

    /**
     * Default sort ordering
     * @var array
     */
    private static $default_sort = ['ID' => 'DESC'];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'NomorResi',
        'Kurir.FirstName' => 'Kurir',
        'BarangJenis',
        'From' => 'Dari',
        'To' => 'Ke',
        'JamPengambilan',
        'TotalHarga',
        'StatusToString' => 'Status Pengiriman',
        'PembayaranToString' => 'Status Pembayaran',
    ];

    /**
     * Defines a default list of filters for the search context
     * @var array
     */
    private static $searchable_fields = [
        'NomorResi',
        'PengirimKota.Title',
        'PengirimKecamatan.Title',
        'PenerimaKota.Title',
        'PenerimaKecamatan.Title',
    ];

    public function onBeforeWrite()
    {
        $pengirimkecamatan = $this->PengirimKecamatan();
        $penerimakecamatan = $this->PenerimaKecamatan();

        $size = $this->Size();
        $type = $this->Type;
        $ongkir = HargaOngkir::get()->filter(
            [
                'DariKotaID' => $pengirimkecamatan->KotaID,
                'DariKotaBagianID' => $pengirimkecamatan->KotaBagianID,
                'KeKotaID' => $penerimakecamatan->KotaID,
                'KeKotaBagianID' => $penerimakecamatan->KotaBagianID,
                'Size' => $size,
                'Type' => $type
            ]
        )->first();
        while (!$ongkir && $size > 0) {
            $size--;
            $ongkir = HargaOngkir::get()->filter([
                'DariKotaID' => $pengirimkecamatan->KotaID,
                'DariKotaBagianID' => $pengirimkecamatan->KotaBagianID,
                'KeKotaID' => $penerimakecamatan->KotaID,
                'KeKotaBagianID' => $penerimakecamatan->KotaBagianID,
                'Size' => $size,
                'Type' => $type
            ])->first();
        }

        $this->TotalHarga = $ongkir ? $ongkir->Harga : 0;
        if ($this->ID == 0) {
            $this->StatusPengiriman = 1;
            if ($this->StatusPembayaran == 0) {
                $this->StatusPembayaran = 1;
            }
            $this->NomorResi = Order::GenerateResi();
        }

        // if ($this->VoucherID != 0) {
        //     $voucher = Voucher::get_by_id($this->VoucherID);
        //     if ($voucher->Jenis == "Rupiah") {
        //         $this->TotalHarga = $ongkir->Harga - $voucher->Potongan;
        //     }
        //     else {
        //         $totalPotongan = $ongkir->Harga * $voucher->Potongan / 100;
        //         if ($this->Voucher()->isMaximumPotongan) {
        //             $totalPotongan = $this->Voucher()->MaximumPotongan;
        //         }
        //         $this->TotalHarga = $ongkir->Harga - $totalPotongan;
        //     }
        // }

        if ($this->isChanged('KurirID')) {
            if ($this->getChangedFields()['KurirID']['before'] != $this->getChangedFields()['KurirID']['after']) {
                if ($this->KurirID != 0) {
                    Notifier::Notify(FCMID::getMemberToken($this->Kurir()), [
                        'Title' => 'Order Baru',
                        'Content' => 'Halo kurir, Anda mendapatkan order baru!',
                        'OrderID' => $this->ID,
                        'Context' => 'NEW_ORDER'
                    ]);
                }
            }
        }

        if ($this->isChanged('StatusPengiriman')) {
            if ($this->getChangedFields()['StatusPengiriman']['before'] != $this->getChangedFields()['StatusPengiriman']['after']) {
                if ($this->StatusPengiriman == 2) {
                    Notifier::Notify(FCMID::getMemberToken($this->Customer()), [
                        'Title' => 'Paket diambil oleh kurir',
                        'Content' => 'Halo '.$this->Customer()->FirstName.', paket Anda telah diambil oleh kurir!',
                        'OrderID' => $this->ID,
                        'Context' => 'ORDER_ON_THE_GO'
                    ]);
                }
                if ($this->StatusPengiriman == 3) {
                    Notifier::Notify(FCMID::getMemberToken($this->Customer()), [
                        'Title' => 'Paket terkirim',
                        'Content' => 'Halo '.$this->Customer()->FirstName.', paket Anda telah terkirim!',
                        'OrderID' => $this->ID,
                        'Context' => 'DELIVERED_ORDER'
                    ]);
                }
            }
        }

        if ($this->isChanged('StatusPembayaran')) {
            if ($this->getChangedFields()['StatusPembayaran']['before'] != $this->getChangedFields()['StatusPembayaran']['after']) {
                if ($this->StatusPembayaran == 3) {
                    Notifier::Notify(FCMID::getMemberToken($this->Customer()), [
                        'Title' => 'Pembayaran diterima',
                        'Content' => 'Halo '.$this->Customer()->FirstName.', pembayaran order Anda sudah diterima!',
                        'OrderID' => $this->ID,
                        'Context' => 'ORDER_PAID'
                    ]);
                }
            }
        }
        parent::onBeforeWrite();
    }

    /**
     * Event handler called after writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onAfterWrite()
    {
        parent::onAfterWrite();
        if ($this->isChanged('StatusPembayaran')) {
            if ($this->StatusPembayaran == 3) {
                $track = OrderTrack::create();
                $track->OrderID = $this->ID;
                $track->Title = "Payment accepted";
                $track->Timestamp = date('Y-m-d H:i:s');
                $track->write();
            }
        }
    }

    public function getCMSFields()
    {
        Requirements::javascript('https://code.jquery.com/jquery-3.6.0.js');
        Requirements::javascript('//cdn.jsdelivr.net/npm/sweetalert2@10');
        Requirements::javascript('OrderJS.js');

        $jam = TimeField::create('JamPengambilan', 'Jam Pengambilan');
        if ($this->ID) {
            $jam = TimeField_Readonly::create('JamPengambilan', 'Jam Pengambilan');
        }

        $kurir = DropdownField::create(
            'KurirID',
            'Kurir',
            Kurir::get()->filter('isGPS', 1)->map('ID', 'FirstName')
        );
        $kurir->setEmptyString('(Select Kurir)');

        $StatusPengiriman = DropdownField::create(
            'StatusPengiriman',
            'Status Pengiriman',
            [
                1 => 'New Order',
                2 => 'Order On The Go',
                3 => 'Delivered Order',
                5 => 'Canceled Order'
            ]
        );

        $StatusPembayaran = DropdownField::create(
            'StatusPembayaran',
            'Status Pembayaran',
            [
                1 => 'Pending',
                2 => 'Waiting For Approval',
                3 => 'Paid',
            ]
        );

        if ($this->StatusPengiriman == 3 || $this->StatusPengiriman == 5) {
            $StatusPengiriman = TextField::create(
                'StatusPengiriman2',
                'Status Pengiriman'
            )
            ->setValue($this->StatusToString())
            ->setReadonly(true);
        }

        if ($this->StatusPembayaran == 3) {
            $StatusPembayaran = TextField::create(
                'StatusPembayaran2',
                'Status Pembayaran'
            )
            ->setValue($this->PembayaranToString())
            ->setReadonly(true);
        }

        $button = LiteralField::create('Bukti Pembayaran', '');
        if ($this->PaymentMethod == 'Manual Transfer') {
            $button = LiteralField::create('Bukti Pembayaran', '<button id="lihat" class="btn action btn-outline-info lihat" style="margin-bottom: 10px" onclick="showImage(`'.$this->BuktiPembayaran()->URL.'`)">Lihat Bukti Pembayaran</button>');
        }

        return FieldList::create(
            LiteralField::create('Judul Status', '<h1> Data Order </h1>'),
            $button,
            TextField::create('NomorResi', 'Nomor Resi')->setAttribute('readonly', 'true'),
            LiteralField::create('TypeField', '
                <style>
                    .input {
                        padding: 5px 2px;
                        border: 1px solid #b3b3b3;
                        border-radius: 4px;
                    }
                </style>
                <div class="form-group field">
                    <label class="form__field-label">Type</label>
                    <div class="form__field-holder">
                        <input type="text" class="text" value="'.$this->TypeToString().'" readonly="true">
                    </div>
                </div>
            '),
            $jam,
            TextField::create('TotalHarga', 'Total Harga')->setAttribute('readonly', 'true'),
            LiteralField::create('VoucherField', '
                <style>
                    .input {
                        padding: 5px 2px;
                        border: 1px solid #b3b3b3;
                        border-radius: 4px;
                    }
                </style>
                <div class="form-group field">
                    <label class="form__field-label">Voucher</label>
                    <div class="form__field-holder">
                        <input type="text" class="text" value="'.$this->Voucher()->Kode.'" readonly="true">
                    </div>
                </div>
            '),
            DropdownField::create(
                'IsFragile',
                'Mudah pecah?',
                [
                    0 => 'No',
                    1 => 'Yes',
                ]
            ),
            $StatusPengiriman,
            $StatusPembayaran,
            LiteralField::create('Judul Pengirim', '<h1> Data Pengirim </h1>'),
            TextField::create(
                'PengirimNama',
                'Nama'
            ),
            TextField::create(
                'PengirimNoTelp',
                'Nomor Telpon'
            ),
            TextField::create(
                'PengirimEmail',
                'Email'
            ),
            DropdownField::create(
                'PengirimKotaID',
                'Kota',
                Kota::get()->map('ID', 'Title')
            ),
            DropdownField::create(
                'PengirimKecamatanID',
                'Kecamatan',
                Kecamatan::get()->map('ID', 'Title')
            ),
            TextField::create(
                'PengirimKodePos',
                'Kode Pos'
            ),
            TextareaField::create(
                'PengirimAlamat',
                'Detail Alamat'
            ),
            LiteralField::create('Judul Penerima', '<h1> Data Penerima </h1>'),
            TextField::create(
                'PenerimaNama',
                'Nama'
            ),
            TextField::create(
                'PenerimaNoTelp',
                'Nomor Telpon'
            ),
            TextField::create(
                'PenerimaEmail',
                'Email'
            ),
            DropdownField::create(
                'PenerimaKotaID',
                'Kota',
                Kota::get()->map('ID', 'Title')
            ),
            DropdownField::create(
                'PenerimaKecamatanID',
                'Kecamatan',
                Kecamatan::get()->map('ID', 'Title')
            ),
            TextField::create(
                'PenerimaKodePos',
                'Kode Pos'
            ),
            TextareaField::create(
                'PenerimaAlamat',
                'Detail Alamat'
            ),
            LiteralField::create('Judul Barang', '<h1> Data Barang </h1>'),
            TextField::create(
                'BarangJenis',
                'Jenis'
            ),
            TextField::create(
                'BarangBerat',
                'Berat (gram)'
            ),
            TextField::create(
                'BarangVolume',
                'Volume (cm3)'
            ),
            LiteralField::create('Judul Kurir', '<h1> Data Kurir </h1>'),
            $kurir
        );
    }

    public function From()
    {
        return $this->PengirimKota->Title . ', ' . $this->PengirimKecamatan->Title;
    }

    public function To()
    {
        return $this->PenerimaKota->Title . ', ' . $this->PenerimaKecamatan->Title;
    }

    public function StatusToString()
    {
        switch ($this->StatusPengiriman) {
            case 1 :
                return 'New Order';
                break;

            case 2 :
                    return 'Order On The Go';
                    break;

            case 3 :
                    return 'Delivered Order';
                    break;

            case 5 :
                    return 'Canceled Order';
                    break;

            default:
                return 'Unknown Order [Danger]';
                break;
        }
    }

    public function PembayaranToString()
    {
        switch ($this->StatusPembayaran) {
            case 1 :
                return 'Pending';
                break;

            case 2 :
                return 'Waiting for Approval';
                break;

            case 3 :
                return 'Paid';
                break;

            default:
                return 'Unknown Order [Danger]';
                break;
        }
    }

    public function TypeToString()
    {
        switch ($this->Type) {
            case 0 :
                return 'Express';
                break;

            default:
                return 'Sameday';
                break;
        }
    }

    static function GenerateResi()
    {
        $nomor = date('YmdGis') . rand(0, 999);
        $resi = Order::get()->filter(['NomorResi'=>$nomor])->first();
        while ($resi) {
            $nomor = date('YmdGis') . rand(0, 999);
            $resi = Order::get()->filter(['NomorResi'=>$nomor])->first();
        }
        return $nomor;
    }

    public function Fragile()
    {
        if ($this->IsFragile == 0) {
            return 'No';
        }
        else {
            return 'Yes';
        }
    }

    public function getRating()
    {
        $arr = ArrayList::create();
        $rate = $this->Rate;
        for ($i=0; $i < $rate; $i++) {
            $object = DataObject::create();
            $object->Star = '1';
            $arr->push($object);
        }
        return $arr;
    }

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['Created'] = $this->Created;
        $arr['NomorResi'] = $this->NomorResi;
        $arr['Type'] = $this->TypeToString();

        $arr['PengirimNama'] = $this->PengirimNama;
        $arr['PengirimEmail'] = $this->PengirimEmail;
        $arr['PengirimNoTelp'] = $this->PengirimNoTelp;
        $arr['PengirimAlamat'] = $this->PengirimAlamat;
        $arr['PengirimLat'] = $this->PengirimLat;
        $arr['PengirimLong'] = $this->PengirimLong;
        $arr['PengirimNote'] = $this->PengirimNote;
        $arr['PenerimaNama'] = $this->PenerimaNama;
        $arr['PenerimaEmail'] = $this->PenerimaEmail;
        $arr['PenerimaNoTelp'] = $this->PenerimaNoTelp;
        $arr['PenerimaAlamat'] = $this->PenerimaAlamat;
        $arr['PenerimaLat'] = $this->PenerimaLat;
        $arr['PenerimaLong'] = $this->PenerimaLong;
        $arr['PenerimaNote'] = $this->PenerimaNote;

        $arr['PengirimKota'] = $this->PengirimKota()->toArray();
        $arr['PengirimKecamatan'] = $this->PengirimKecamatan()->toArray();
        $arr['PenerimaKota'] = $this->PenerimaKota()->toArray();
        $arr['PenerimaKecamatan'] = $this->PenerimaKecamatan()->toArray();

        $arr['BarangJenis'] = $this->BarangJenis;
        $arr['BarangBerat'] = $this->BarangBerat;
        $arr['BarangVolume'] = $this->BarangVolume;
        $arr['IsFragile'] = $this->IsFragile;
        $arr['TotalHarga'] = $this->TotalHarga;
        $arr['JamPengambilan'] = $this->JamPengambilan;
        $arr['StatusPengiriman'] = $this->StatusPengiriman;
        $arr['StatusPembayaran'] = $this->StatusPembayaran;

        $arr['Kurir'] = null;
        if ($this->KurirID > 0) {
            $arr['Kurir'] = $this->Kurir()->toArray();
        }
        $arr['Customer'] = null;
        if ($this->CustomerID > 0) {
            $arr['Customer'] = $this->Customer()->toArray();
        }
        $arr['Voucher'] = null;
        $arr['Rules'] = null;
        if ($this->VoucherID > 0) {
            $voucher = Voucher::get_by_id($this->VoucherID);
            if ($voucher) {
                $arr['Voucher'] = $voucher->toArray();

                $rules = [];
                if ($voucher->isMaximumPotongan) {
                    $rules['MaximumPotongan'] = $voucher->MaximumPotongan;
                }
                else {
                    $rules = new stdClass();
                }
                $arr['Rules'] = $rules;
            }
        }
        $arr['PembayaranAccepted'] = $this->PembayaranAccepted;
        $arr['KurirAmbil'] = $this->KurirAmbil;
        $arr['KurirKirim'] = $this->KurirKirim;
        $arr['BuktiPengiriman'] = $this->BuktiPengiriman()->AbsoluteURL;
        return $arr;
    }

    public function getAllTrackHistory()
    {
        $track = OrderTrack::get()->filter([
            'OrderID' => $this->ID
        ]);

        $arrTrack = ArrayList::create();
        $count = 0;
        foreach ($track as $key => $value) {
            $temp = DataObject::create();
            $temp->Count = $count;
            $count++;
            $temp->Timestamp = date('d M H:i', strtotime($value->Timestamp));
            $temp->Done = true;
            $temp->Desc = $value->Title;
            $arrTrack->push($temp);
        }

        if (count($arrTrack) == 0) {
            $temp = DataObject::create();
            $temp->Count = 0;
            $temp->Timestamp = date('d M H:i');
            $temp->Done = false;
            $temp->Desc = 'Payment accepted';
            $arrTrack->push($temp);
        }

        if (count($arrTrack) == 1) {
            $temp = DataObject::create();
            $temp->Count = 1;
            $temp->Timestamp = date('d M H:i');
            $temp->Done = false;
            $temp->Desc = 'Courier has received your package';
            $arrTrack->push($temp);
        }

        if (count($arrTrack) == 2) {
            $temp = DataObject::create();
            $temp->Count = 2;
            $temp->Timestamp = date('d M H:i');
            $temp->Done = false;
            $temp->Desc = 'Your package has been delivered';
            $arrTrack->push($temp);
        }

        return $arrTrack;
    }

    public function Size()
    {
        if ($this->BarangVolume == 20*20*8) {
            return 1;
        }
        if ($this->BarangVolume == 30*22*12) {
            return 2;
        }
        if ($this->BarangVolume == 35*30*25) {
            return 3;
        }
        return 0;
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class OrderAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'Order',
        'OrderTrack'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'order-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Order';

    private static $menu_icon_class = 'font-icon-clipboard-pencil';

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        // make sure to check if the modelClass matches the object you want to edit
        // otherwise, the config will get applied to all models managed
        // by this ModelAdmin instance
        $form
          ->Fields()
          ->fieldByName($this->sanitiseClassName($this->modelClass))
          ->getConfig()
          ->removeComponentsByType('GridFieldDeleteAction')
          ->removeComponentsByType('GridFieldAddNewButton');

        return $form;
    }
}
