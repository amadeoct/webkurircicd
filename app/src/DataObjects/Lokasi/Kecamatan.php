<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Kecamatan extends DataObject
{
    /**
     * Singular name for CMS
     *  @var string
     */
    private static $singular_name = 'Kecamatan';

    /**
     * Plural name for CMS
     *  @var string
     */
    private static $plural_name = 'Kecamatan';

    private static $db = [
        'Title' => 'Varchar(100)'
    ];

    private static $has_one = [
        'Kota' => Kota::class,
        'KotaBagian' => KotaBagian::class,
    ];

    private static $has_many = [
        'KodePos' => KodePos::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'Title',
        'Kota.Title' => 'Kota',
        'KotaBagian.Title' => 'Bagian',
    ];

    public function getCMSFields()
    {
        return FieldList::create(
            DropdownField::create(
                'KotaID',
                'Kota',
                Kota::get()->map('ID', 'Title')
            ),
            DropdownField::create(
                'KotaBagianID',
                'Bagian',
                KotaBagian::get()->map('ID', 'Title')
            ),
            TextField::create(
                'Title',
                'Judul Kecamatan'
            )
        );
    }

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['Title'] = $this->Title;
        $arr['Bagian'] = $this->KotaBagian()->Title;
        return $arr;
    }
}
