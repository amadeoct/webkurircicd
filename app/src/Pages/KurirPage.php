<?php

use SilverStripe\ORM\ArrayList;
use SilverStripe\Assets\Upload;
use SilverStripe\Assets\Image;
use SilverStripe\Security\Member;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Security\Security;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;
use SilverStripe\ORM\PaginatedList;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class KurirPage extends Page
{
    public function CountJobOrder()
    {
        $kurir = Member::currentUser();
        $order = Order::get()->filter(['KurirID' => $kurir->ID, 'StatusPengiriman' => 1]);
        return count($order);
    }

    public function CountOnGoingOrder()
    {
        $kurir = Member::currentUser();
        $order = Order::get()->filter(['KurirID' => $kurir->ID, 'StatusPengiriman' => 2]);
        return count($order);
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class KurirPageController extends PageController
{
    public function doInit()
    {
        parent::doInit();
    }

    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'Dashboard',
        'ForgotPassword',
        'SendForgotEmail',
        'RepairPassword',
        'ProsesrepairPassword',
        'Login',
        'ProsesLogin',
        'LogOut',
        'JobOrder',
        'AmbilBarang',
        'OnGoingOrder',
        'UploadBukti',
        'FinishedOrder'
    ];

    public function index()
    {
        return $this->redirect('kurir/Dashboard');
    }

    public function Dashboard()
    {
        $Flash = $this->getRequest()->getSession()->get('Flash');
        if ($Flash) {
            $this->getRequest()->getSession()->clear('Flash');
        }

        $kurir = Member::currentUser();
        if (!$kurir) {
            return $this->redirect(KurirPage::get()->first()->Link().'Login');
        }
        $order = Order::get()->filter(['KurirID' => $kurir->ID, 'StatusPengiriman' => 3, 'Rate:GreaterThan' => 0])->sort('ID', 'DESC');
        return ['Flash' => $Flash, 'Order' => $order, 'BC' => 'Dashboard'];
    }

    public function ForgotPassword()
    {
        return ['Forgot' => true];
    }

    public function SendForgotEmail()
    {
        $email = isset($_POST['Email']) ? $_POST['Email'] : null;
        if (!$email) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Mohon masukkan email Anda'
            ]);
        }

        $kurir = Kurir::get()->filter('Email', $email)->first();
        if (!$kurir) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Email tidak ditemukan, gunakan email Anda yang terdaftar pada kami'
            ]);
        }

        $page = KurirPage::get()->first();
        $config = SiteConfig::current_site_config();
        $url = Director::absoluteBaseURL();
        $from = 'no-reply@webkurir.com';
        $to = $kurir->Email;
        $link = $page->AbsoluteLink() . 'RepairPassword/' . $kurir->ID;
        $subject = $config->Title . ' - Repair Password';

        $email = Email::create()
        ->setData([
            'Link'=> $link,
            'FirstName' => $kurir->FirstName,
            'Surname' => $kurir->Surname,
            'Title' => $config->Title,
            'Image' => $config->Logo->AbsoluteURL
        ])
        ->setHTMLTemplate('Email\\ForgotPasswordEmail')
        ->setFrom($from)
        ->setTo($to)
        ->setSubject($subject);

        if (substr($url, 0, 16) != 'http://localhost') {
            // SMTP Configuration
            $transport = (new Swift_SmtpTransport($config->SMTPHost, $config->SMTPPort))
            ->setUsername($config->SMTPUsername)
            ->setPassword($config->SMTPPassword);
            $mailer = new Swift_Mailer($transport);
            $message = (new Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $email->render()->Body,
                'text/html' // Mark the content-type as HTML
            );
            $result = $mailer->send($message);
        }
        else {
            $email->send();
        }

        return json_encode([
            'Status' => 200,
            'Message' => 'Sukses, Silahkan cek email kamu untuk melanjutkan proses repair password'
        ]);
    }

    public function RepairPassword()
    {
        $id = $this->getRequest()->param('ID');
        $page = KurirPage::get()->first();

        if (!$id) {
            return $this->redirect("");
        }

        $kurir = Kurir::get_by_id($id);

        if (!$kurir) {
            return $this->HttpError("404");
        }

        return ['Kurir' => $kurir];
    }

    public function ProsesRepairPassword()
    {
        $email = isset($_POST['Email']) ? $_POST['Email'] : null;
        $password = isset($_POST['Password']) ? $_POST['Password'] : null;
        $cpassword = isset($_POST['CPassword']) ? $_POST['CPassword'] : null;

        if (!$email || !$password || !$cpassword) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Email, Password dan Confirm Password tidak boleh kosong'
            ]);
        }

        if ($password == '' || $cpassword == '') {
            return json_encode([
                'Status' => 417,
                'Message' => 'Email, Password dan Confirm Password tidak boleh kosong'
            ]);
        }

        if (strlen($password) < 8) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Panjang Password harus minimal 8 karakter'
            ]);
        }

        if ($password != $cpassword) {
            return json_encode([
                'Status' => 417,
                'Message' => 'Password dan Confirm Password harus sama'
            ]);
        }

        $kurir = Kurir::get()->filter('Email', $email)->first();
        if (!$kurir) {
            // Hampir tidak mungkin terjadi
            return json_encode([
                'Status' => 404,
                'Message' => 'Email tidak ditemukan'
            ]);
        }

        $kurir->Password = $password;
        $kurir->write();

        return json_encode([
            'Status' => 201,
            'Message' => 'Sukses Repair Password, Klik Ok lalu Anda akan diarahkan ke Login.'
        ]);
    }

    public function Login()
    {
        $Flash = $this->getRequest()->getSession()->get('Flash');
        if ($Flash) {
            $this->getRequest()->getSession()->clear('Flash');
        }
        return ['Flash' => $Flash];
    }

    public function ProsesLogin($data)
    {
        $session = $this->getRequest()->getSession();
        $email = $_POST['Email'];
        $password = $_POST['Password'];
        if (!$email || !$password) {
            $session->set('Flash', 'Mohon isi Email dan Password');
            return $this->RedirectBack();
        }

        $result = ValidationResult::create();
        $auth = new MemberAuthenticator();
        $user = $auth->authenticate($_REQUEST, $data);

        if (!$user) {
            $session->set('Flash', 'Email/Password Salah');
            return $this->RedirectBack();
        }

        if ($user->ClassName == "Customer") {
            $session->set('Flash', 'Anda tidak bisa Login disini! Coba Login as Customer');
            return $this->RedirectBack();
        }
        $identityStore = Injector::inst()->get(IdentityStore::class);
        $identityStore->logIn($user);

        return $this->redirect(KurirPage::get()->first()->Link() . 'Dashboard');
    }

    public function LogOut()
    {
        Injector::inst()->get(IdentityStore::class)->logOut();

        return $this->redirect(KurirPage::get()->first()->Link() . 'Login');
    }

    public function JobOrder()
    {
        $Sukses = $this->getRequest()->getSession()->get('Sukses');
        if ($Sukses) {
            $this->getRequest()->getSession()->clear('Sukses');
        }

        $member = Member::currentUser();
        if (!$member) {
            return $this->redirect(KurirPage::get()->first()->Link() . 'Login');
        }
        if ($member->ClassName == 'Customer') {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }
        $order = Order::get()->filter(['KurirID' => $member->ID, 'StatusPengiriman' => 1])->sort('LastEdited', 'DESC');
        $val = 0;
        if (isset($_GET['Date']) && $_GET['Date'] != '') {
            $val = $_GET['Date'];
            $temp = ArrayList::create();
            foreach ($order as $key) {
                $year = date('Y', strtotime($key->Created));
                $month = date('m', strtotime($key->Created));

                if ($val == $year . $month) {
                    $temp->push($key);
                }
            }
            $order = $temp;
        }

        $order = PaginatedList::create(
            $order,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('p');

        $order_all = Order::get()->filter(['KurirID' => $member->ID])->sort('StatusPengiriman', 'ASC');
        $filter = ArrayList::create();
        foreach ($order_all as $key) {
            $year = date('Y', strtotime($key->Created));
            $month = date('m', strtotime($key->Created));
            $ada = false;
            foreach ($filter as $value) {
                if ($year == $value->Year && $month == $value->Month) {
                    $ada = true;
                }
            }
            if (!$ada) {
                $object = DataObject::create();
                $object->Value = $year . $month;
                $object->Year = $year;
                $object->Month = $month;
                $filter->push($object);
            }
        }

        return ['Order' => $order,'DateFilter' => $filter, 'Value' => $val, 'Sukses' => $Sukses, 'BC' => 'JobOrder'];
    }

    public function AmbilBarang()
    {
        date_default_timezone_set("Asia/Jakarta");
        $resi = $this->getRequest()->param('ID');

        $order = Order::get()->filter('NomorResi', $resi)->first();
        $order->KurirAmbil = date("h:i:sa");
        $order->StatusPengiriman = 2;
        $order->write();

        // var_dump($order);die();

        $this->getRequest()->getSession()->set('Sukses', 'Sukses Ambil Barang');

        return $this->RedirectBack();
    }

    public function OnGoingOrder()
    {
        $Sukses = $this->getRequest()->getSession()->get('Sukses');
        if ($Sukses) {
            $this->getRequest()->getSession()->clear('Sukses');
        }

        $Error = $this->getRequest()->getSession()->get('Error');
        if ($Error) {
            $this->getRequest()->getSession()->clear('Error');
        }

        $member = Member::currentUser();
        if (!$member) {
            return $this->redirect(KurirPage::get()->first()->Link() . 'Login');
        }
        if ($member->ClassName == 'Customer') {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }
        $order = Order::get()->filter(['KurirID' => $member->ID, 'StatusPengiriman:GreaterThan' => 1, 'Rate' => 0])->sort('StatusPengiriman', 'ASC')->sort('LastEdited', 'DESC');
        $val = 0;
        if (isset($_GET['Date']) && $_GET['Date'] != '') {
            $val = $_GET['Date'];
            $temp = ArrayList::create();
            foreach ($order as $key) {
                $year = date('Y', strtotime($key->Created));
                $month = date('m', strtotime($key->Created));

                if ($val == $year . $month) {
                    $temp->push($key);
                }
            }
            $order = $temp;
        }

        $order = PaginatedList::create(
            $order,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('p');

        $order_all = Order::get()->filter(['KurirID' => $member->ID, 'StatusPengiriman:GreaterThan' => 1, 'Rate' => 0]);
        $filter = ArrayList::create();
        foreach ($order_all as $key) {
            $year = date('Y', strtotime($key->Created));
            $month = date('m', strtotime($key->Created));
            $ada = false;
            foreach ($filter as $value) {
                if ($year == $value->Year && $month == $value->Month) {
                    $ada = true;
                }
            }
            if (!$ada) {
                $object = DataObject::create();
                $object->Value = $year . $month;
                $object->Year = $year;
                $object->Month = $month;
                $filter->push($object);
            }
        }

        return ['Order' => $order,'DateFilter' => $filter, 'Value' => $val, 'Sukses' => $Sukses, 'Error' => $Error, 'BC' => 'OnGoingOrder'];
    }

    public function UploadBukti()
    {
        date_default_timezone_set("Asia/Jakarta");
        $session = $this->getRequest()->getSession();
        $photo = isset($_FILES['Photo']) ? $_FILES['Photo'] : null;
        if (!$photo || $photo['name'] == '') {
            $session->set('Error', 'Mohon isi Foto Bukti Pengiriman');
            return $this->RedirectBack();
        }

        $extension = $photo['type'];
        $extension = substr($extension, strpos($extension, "/")+1);
        if ($extension != 'png' && $extension != 'jpg' && $extension != 'jpeg') {
            $session->set('Error', 'Extension not allowed');
            return $this->RedirectBack();
        }
        $nomorresi = $_POST['NomorResi'];
        $order = Order::get()->filter('NomorResi', $nomorresi)->first();

        $member = Member::currentUser();
        if ($member) {
            if ($member->ClassName == 'Kurir') {
                $upload = new Upload();
                $image = Image::create();

                if (!$upload->loadIntoFile($photo, $image, 'BuktiPembayaran')) {
                    $this->getRequest()->getSession()->set('Error', 'Failed Upload Image');
                    return $this->redirect(KurirPage::get()->first()->Link() . 'JobOrder');
                }

                $order->BuktiPengiriman = $image;
                $order->KurirKirim = date("h:i:sa");
                $order->StatusPengiriman = 3;
                $order->write();

                $session->set('Sukses', 'Sukses Upload Bukti Pengiriman');

                return $this->RedirectBack();
            }
        }
        return $this->redirect(KurirPage::get()->first()->Link() . 'Login');
    }

    public function FinishedOrder()
    {
        $member = Member::currentUser();
        if (!$member) {
            return $this->redirect(KurirPage::get()->first()->Link() . 'Login');
        }
        if ($member->ClassName == 'Customer') {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }
        $order = Order::get()->filter(['KurirID' => $member->ID, 'StatusPengiriman' => 3, 'Rate:GreaterThan' => 0])->sort('LastEdited', 'DESC');
        $val = 0;
        if (isset($_GET['Date']) && $_GET['Date'] != '') {
            $val = $_GET['Date'];
            $temp = ArrayList::create();
            foreach ($order as $key) {
                $year = date('Y', strtotime($key->Created));
                $month = date('m', strtotime($key->Created));

                if ($val == $year . $month) {
                    $temp->push($key);
                }
            }
            $order = $temp;
        }

        $order = PaginatedList::create(
            $order,
            $_REQUEST
        )->setPageLength(10)->setPaginationGetVar('p');

        $order_all = Order::get()->filter(['KurirID' => $member->ID, 'StatusPengiriman' => 3, 'Rate:GreaterThan' => 0]);
        $filter = ArrayList::create();
        foreach ($order_all as $key) {
            $year = date('Y', strtotime($key->Created));
            $month = date('m', strtotime($key->Created));
            $ada = false;
            foreach ($filter as $value) {
                if ($year == $value->Year && $month == $value->Month) {
                    $ada = true;
                }
            }
            if (!$ada) {
                $object = DataObject::create();
                $object->Value = $year . $month;
                $object->Year = $year;
                $object->Month = $month;
                $filter->push($object);
            }
        }

        return ['Order' => $order,'DateFilter' => $filter, 'Value' => $val, 'BC' => 'FinishedOrder'];
    }
}
