<?php

use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\View\Requirements;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ArrayList;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class TopUpRequest extends DataObject
{
    private static $db = [
        'BankName' => 'Varchar(255)',
        'AccountName' => 'Varchar(255)',
        'AccountNumber' => 'Varchar(255)',
        'TransactionDate' => 'Datetime',
        'Amount' => 'Int',
        'Status' => 'Int'
    ];

    private static $has_one = [
        'Customer' => Customer::class,
        'Image' => Image::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'ID',
        'Customer.FirstName',
        'StatusToString' => 'Status'
    ];

    /**
     * Defines a default list of filters for the search context
     * @var array
     */
    private static $searchable_fields = [
        'Customer.FirstName'
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->isChanged('Status')) {
            if ($this->getChangedFields()['Status']['before'] != $this->getChangedFields()['Status']['after']) {
                if ($this->Status == 1) {
                    Notifier::Notify(FCMID::getMemberToken($this->Customer()), [
                        'Title' => 'Pembayaran diterima',
                        'Content' => 'Halo '.$this->Customer()->FirstName.', pembayaran top up Anda telah diterima dan saldo Anda telah ditambahkan!',
                        'OrderID' => $this->ID,
                        'Context' => 'TOPUP_PAID'
                    ]);

                    $customer = $this->Customer();
                    $customer->Wallet += $this->Amount;
                    $customer->write();
                }
            }
        }
    }

    /**
     * CMS Fields
     * @return FieldList
     */
    public function getCMSFields()
    {
        Requirements::javascript('//cdn.jsdelivr.net/npm/sweetalert2@10');
        Requirements::javascript('TopUpRequestJS.js');
        $fields = parent::getCMSFields();
        $fields->removeByName([
            'CustomerID',
            'Image',
            'Status'
        ]);
        $fields->makeFieldReadonly('Amount');
        $fields->addFieldToTab(
            'Root.Main',
            LiteralField::create('Button', '<button id="lihat" class="btn action btn-outline-info lihat" style="margin-bottom: 10px" onclick="showImage(`'.$this->Image()->URL.'`)">Lihat Bukti Pembayaran</button>'),
            'BankName'
        );
        $arr = ArrayList::create();
        $arr->push($this->Customer());
        $fields->addFieldToTab(
            'Root.Main',
            GridField::create(
                'Customer',
                'Customer',
                $arr
            )
        );
        if ($this->Status != 1) {
            $fields->addFieldToTab(
                'Root.Main',
                DropdownField::create(
                    'Status',
                    'Status',
                    [
                        0 => 'Pending',
                        1 => 'Paid',
                        2 => 'Cancel'
                    ]
                )
            );
        }
        else {
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create(
                    'StatusReadonly',
                    'Status'
                )
                ->setValue($this->StatusToString())
                ->setReadonly(true)
            );
        }
        return $fields;
    }

    public function StatusToString()
    {
        switch ($this->Status) {
            case 0:
                return "Pending";
                break;
            case 1:
                return "Paid";
                break;
            default:
                return "Cencel";
                break;
        }
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class TopUpRequestAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'TopUpRequest'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'top-up-request';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Top Up Request';

    private static $menu_icon_class = 'font-icon-tag';


}
