<?php

use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class Kota extends DataObject
{
    /**
     * Singular name for CMS
     *  @var string
     */
    private static $singular_name = 'Kota';

    /**
     * Plural name for CMS
     *  @var string
     */
    private static $plural_name = 'Kota';

    private static $db = [
        'Title' => 'Varchar(100)'
    ];

    private static $has_many = [
        'Kecamatan' => Kecamatan::class
    ];

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['Title'] = $this->Title;
        return $arr;
    }
}
