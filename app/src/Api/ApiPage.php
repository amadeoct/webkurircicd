<?php

use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\Member;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class ApiPage extends Page
{
    public function CheckClientID()
    {
        if (!isset($_SERVER['HTTP_CLIENTID'])) {
            return json_encode([
                'Status' => 419,
                'Message' => 'Unauthorized'
            ]);
        }

        $apps = Apps::get()->filter(['ClientID' => $_SERVER['HTTP_CLIENTID']])->first();

        if (!$apps) {
            return json_encode([
                'Status' => 419,
                'Message' => 'Unauthorized'
            ]);
        }

        return false;
    }

    public function CheckAccessToken()
    {
        if (!isset($_SERVER['HTTP_ACCESSTOKEN'])) {
            return json_encode([
                'Status' => 418,
                'Message' => 'Unauthorized no AccessToken'
            ]);
        }

        $token = AccessToken::get()->filter(['Token' => $_SERVER['HTTP_ACCESSTOKEN']])->first();

        if (!$token) {
            return json_encode([
                'Status' => 418,
                'Message' => 'Unauthorized AccessToken not found'
            ]);
        }

        return false;
    }

    static function showMessage($code=200, $message="", $arr=array(), $json=true){
        //1xx is all about information
        //2xx is all about success
        //3xx is all about redirection
        //4xx is all about client errors
        //5xx is all about service errors

        $msg = self::getHttpMessage($code);
        if($message){
            $msg = $message;
        }

        $arr_return = array('Status' => $code, 'Message' => $msg);
        if(count($arr) && sizeof($arr)){
            $arr_return['Data'] = [];
            $arr_return['Data'] += $arr;
        }

        if($json){
            //return json_encode($arr_return, JSON_NUMERIC_CHECK);
            return json_encode($arr_return);
        }
        else{
            return $arr_return;
        }
    }
    static function getHttpMessage($code){
        $status_codes = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Request Range Not Satisfiable',
            417 => 'Expectation Failed',
            422 => 'Unprocessable Entity',
            429 => 'Too Many Requests',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
        );
        return isset($status_codes[$code]) ? $status_codes[$code] : '';
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class ApiPageController extends PageController
{
    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        'getSiteConfig',
        'getNews',
        'getLocation',
        'getHomeSlides',
        'getVoucher'
    ];

    public function doInit()
    {
        parent::doInit();
    }

    public function getSiteConfig()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $siteconfig = SiteConfig::current_site_config();
        return self::showMessage(200, 'Sukses Get Siteconfig', [
            'SiteConfig' => [
                'Title' => $siteconfig->Title,
                'LogoURL' => $siteconfig->Logo()->AbsoluteURL,
                'LogoTransparentURL' => $siteconfig->LogoTransparent()->AbsoluteURL,
                'TodaysQuote' => $siteconfig->TodaysQuote,
                'City' => $siteconfig->City,
                'Address' => $siteconfig->Address,
                'Phone' => $siteconfig->Phone,
                'AtasNama' => $siteconfig->AtasNama,
                'NoRek' => $siteconfig->NoRek,
                'Bank' => $siteconfig->Bank
            ]
        ]);
    }

    public function getHomeSlides()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $SortBy = isset($_GET['SortBy']) ? $_GET['SortBy'] : 'ID';
        $SortType = isset($_GET['SortType']) ? $_GET['SortType'] : 'DESC';
        $Limit = isset($_GET['Limit']) ? $_GET['Limit'] : 10;
        $Offset = isset($_GET['Offset']) ? $_GET['Offset'] : 0;

        $slides = Slides::get()->sort($SortBy, $SortType)->limit($Limit, $Offset);
        $arrSlides = [];
        foreach ($slides as $key => $value) {
            $arrSlides[] = $value->toArray();
        }

        return self::showMessage(200, 'Sukses Get HomeSlides', [
            'Slides' => $arrSlides
        ]);
    }

    public function getNews()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $id = $this->getRequest()->param('ID');
        if ($id) {
            $post = Post::get_by_id($id);
            if (!$post) {
                return self::showMessage(404);
            }

            return self::showMessage(200, 'Sukses Get News', [
                'News' => $post->toArray()
            ]);
        }

        $SortBy = isset($_GET['SortBy']) ? $_GET['SortBy'] : 'ID';
        $SortType = isset($_GET['SortType']) ? $_GET['SortType'] : 'DESC';
        $Limit = isset($_GET['Limit']) ? $_GET['Limit'] : 10;
        $Offset = isset($_GET['Offset']) ? $_GET['Offset'] : 0;

        $post = Post::get()->sort($SortBy, $SortType)->limit($Limit, $Offset);
        $arr_post = [];
        foreach ($post as $key => $value) {
            $arr_post[] = $value->toArray();
        }

        return self::showMessage(200, 'Sukses Get News', [
            'News' => $arr_post
        ]);
    }

    public function getLocation()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        // $keyword = isset($_REQUEST['Keyword']) ? $_REQUEST['Keyword'] : '';
        // if (strlen($keyword) < 3) {
        //     return self::showMessage(400, "Min keyword with 3 characters");
        // }

        $arrData = [];
        $kecamatan = Kecamatan::get();
        foreach ($kecamatan as $key => $value) {
            $arrData[] = $value->Kota()->Title . ', ' . $value->Title;
        }

        return self::showMessage(200, "Sukses Get Location",[
            'Location' => $arrData
        ]);
    }

    public function getVoucher()
    {
        if ($this->CheckClientID()) {
            return $this->CheckClientID();
        }

        $id = $this->getRequest()->param('ID');
        if ($id) {
            $voucher = Voucher::get_by_id($id);
            if (!$voucher) {
                return self::showMessage(404);
            }

            return self::showMessage(200, 'Sukses Get Voucher', [
                'Voucher' => $voucher->toArray()
            ]);
        }

        $code = isset($_GET['Code']) ? $_GET['Code'] : '';
        $SortBy = isset($_GET['SortBy']) ? $_GET['SortBy'] : 'ID';
        $SortType = isset($_GET['SortType']) ? $_GET['SortType'] : 'DESC';
        $Limit = isset($_GET['Limit']) ? $_GET['Limit'] : 10;
        $Offset = isset($_GET['Offset']) ? $_GET['Offset'] : 0;

        if (isset($_GET['Code'])) {
            $voucher = Voucher::get()->filter([
                'Kode:nocase' => $code
            ])->first();

            if (!$voucher) {
                return self::showMessage(404, 'Voucher not found');
            }
            if (strtotime($voucher->Valid) > strtotime('now')) {
                return self::showMessage(400, 'Voucher not activated yet');
            }
            if (strtotime($voucher->Expires) < strtotime('now')) {
                return self::showMessage(400, 'Voucher is already expired');
            }

            $token = $_SERVER['HTTP_ACCESSTOKEN'];
            $token = AccessToken::get()->filter('Token', $token)->first();
            $member = Member::get_by_id($token->MemberID);
            $rules = [];
            if ($member) {
                $temp = [];
                if ($voucher->isMinimumPembelian) {
                    $temp['MinimumPembelian'] = $voucher->MinimumPembelian;
                }
                if ($voucher->isMaximumPotongan) {
                    $temp['MaximumPotongan'] = $voucher->MaximumPotongan;
                }
                if ($voucher->isLimitPemakaian) {
                    $temp['AvailableLimit'] = $voucher->AvailableLimit($member);
                }
                if ($voucher->isQuota) {
                    $temp['AvailableQuota'] = $voucher->AvailableQuota();
                }
                if ($voucher->isInCity) {
                    $temp['City'] = [];

                    $city = VoucherManyCity::get()->filter('VoucherID', $voucher->ID);
                    foreach ($city as $key => $value) {
                        $temp['City'][] = $value->Kota()->Title;
                    }
                }
                $rules = $temp;
            }

            if ($rules == []) {
                $rules = new stdClass();
            }

            return self::showMessage(200, "Sukses Get Voucher",[
                'Voucher' => $voucher->toArray(),
                'Rules' => $rules
            ]);
        }

        // $voucher = Voucher::get()->filter([
        //     'Kode:StartsWith:nocase' => $code
        // ])->sort($SortBy, $SortType)->limit($Limit, $Offset);
        $voucher = Voucher::get()->sort($SortBy, $SortType)->limit($Limit, $Offset);

        $arrData = [];
        foreach ($voucher as $key => $value) {
            $arrData[] = $value->toArray();
        }

        return self::showMessage(200, "Sukses Get Voucher",[
            'Voucher' => $arrData
        ]);
    }
}
