<?php

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Security\Member;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Assets\Image;


class Customer extends Member
{
    private static $db = [
        'NoTelp' => 'Varchar(255)',
        'Address' => 'Text',
        'KodePos' => 'Int',
        'Status' => 'Int',
        'initImage' => 'Text',
        'Wallet' => 'Int'
    ];

    private static $has_one = [
        'Kota' => Kota::class,
        'Kecamatan' => Kecamatan::class,
        'Image' => Image::class
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->ID < 1) {
            if ($this->Status != 2) {
                $this->Status = 1;
            }
        }
    }

    public function onAfterWrite()
    {
        parent::onAfterWrite();
        if ($this->Image()->exists() && !$this->Image()->isPublished()) {
            $this->Image()->doPublish();
        }
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName([
            'KodePos',
            'Image',
            'initImage',
            'Permissions',
            'DirectGroups',
            'Locale',
            'FailedLoginCount'
        ]);

        $upload = UploadField::create(
            'Image',
            'Image'
        );
        $upload->setAllowedExtensions([
            'jpeg',
            'jpg',
            'png'
        ]);

        $fields->addFieldToTab(
            'Root.Main',
            $upload,
            'FirstName'
        );
        $fields->addFieldToTab(
            'Root.Main',
            TextField::create(
                'KodePos',
                'Kode Pos'
            )
        );
        $fields->addFieldToTab(
            'Root.Main',
            DropdownField::create(
                'Status',
                'Status',
                [
                    1 => 'Belum Terverifikasi',
                    2 => 'Sudah Terverifikasi'
                ]
            ),
            'Email'
        );
        return $fields;
    }

    public function toArray()
    {
        $arr = array();
        $arr['ID'] = $this->ID;
        $arr['FullName'] = $this->FirstName . ' ' . $this->Surname;
        $arr['Email'] = $this->Email;
        $arr['Phone'] = $this->NoTelp;
        $arr['PostCode'] = $this->KodePos;
        $arr['City'] = $this->GetCityDistrict();
        $arr['ImageURL'] = '';
        if ($this->Image()->exists()) {
            $arr['ImageURL'] = $this->Image()->AbsoluteURL;
        }
        else {
            $arr['ImageURL'] = $this->initImage;
        }
        $arr['Wallet'] = $this->Wallet;
        return $arr;
    }

    public function GetCityDistrict()
    {
        if ($this->KotaID == 0) {
            return '';
        }
        return $this->Kota()->Title . ', ' . $this->Kecamatan()->Title;
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class CustomerAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'Customer'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'customer-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Customer';

    private static $menu_icon_class = 'font-icon-torso';
}
