<?php

use SilverStripe\Security\Member;
use SilverStripe\ORM\DataObject;
use SilverStripe\Admin\ModelAdmin;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class AccessToken extends DataObject
{
    private static $db = [
        'Token' => 'Varchar(100)',
        'DeviceID' => 'Varchar(255)',
    ];

    private static $has_one = [
        'Member' => Member::class
    ];

    /**
     * Defines summary fields commonly used in table columns
     * as a quick overview of the data for this dataobject
     * @var array
     */
    private static $summary_fields = [
        'MemberFullName' => 'Member Name',
        'Token',
        'DeviceID'
    ];

    /**
     * Defines a default list of filters for the search context
     * @var array
     */
    private static $searchable_fields = [
        'Token',
        'DeviceID'
    ];

    /**
     * Event handler called before writing to the database.
     *
     * @uses DataExtension->onAfterWrite()
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $token = AccessToken::get()->filter('MemberID', $this->MemberID);
        if (count($token) > 0) {
            foreach ($token as $key => $value) {
                $value->delete();
            }
        }

        $token = md5(uniqid(mt_rand(), true));
        while(AccessToken::isTokenExist($token)){
            $token = md5(uniqid(mt_rand(), true));
        }
        $this->Token = $token;
    }

    public function MemberFullName()
    {
        return $this->Member()->FirstName . ' ' . $this->Member()->Surname;
    }

    static function isTokenExist($token)
    {
        $token = AccessToken::get()->filter(['Token' => $token])->first();
        if(!$token){
            return false;
        }
        else {
            return true;
        }
    }

    static function GetMemberByToken()
    {
        $token = isset($_SERVER['HTTP_ACCESSTOKEN']) ? $_SERVER['HTTP_ACCESSTOKEN'] : null;
        if (!$token) {
            return null;
        }

        $token = AccessToken::get()->filter('Token', $token)->first();
        if ($token) {
            return Member::get_by_id($token->MemberID);
        }
        return null;
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class AccessTokenAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'AccessToken'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'accesstoken-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'AccessToken';
}
