<?php

use SilverStripe\ORM\DataObject;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class VoucherManyCity extends DataObject
{
    private static $has_one = [
        'Voucher' => Voucher::class,
        'Kota' => Kota::class
    ];
}
