<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Security\Member;
use SilverStripe\CMS\Controllers\ContentController;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class OrderPage extends Page
{

}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class OrderPageController extends PageController
{
    public function doInit()
    {
        parent::doInit();
    }

    /**
     * Defines methods that can be called directly
     * @var array
     */
    private static $allowed_actions = [
        //More Like API, Can Be For Later Use
        'getDistrict',
        'getPostCode',
        'setHarga',
        'Order',
        'OrderConfirmation',
        'Track',
        'GetOrderStatusAjax'
    ];

    public function index()
    {
        $Draft = $this->getRequest()->getSession()->get('Draft');
        $draft_object = DataObject::create();
        if ($Draft != null) {
            $this->getRequest()->getSession()->clear('Draft');
            $draft_object->Available = true;

            $draft_object->SenderName = $Draft['sender-Name'];
            $draft_object->SenderEmail = $Draft['sender-Email'];
            $draft_object->SenderPhone = $Draft['sender-Phone'];
            $draft_object->SenderDistrict = $Draft['sender-District'];
            $draft_object->SenderPostCode = $Draft['sender-postcode'];
            $draft_object->SenderAddressDetail = $Draft['sender-address-details'];

            $draft_object->ReceiverName = $Draft['Receiver-Name'];
            $draft_object->ReceiverEmail = $Draft['Receiver-Email'];
            $draft_object->ReceiverPhone = $Draft['Receiver-Phone'];
            $draft_object->ReceiverDistrict = $Draft['Receiver-District'];
            $draft_object->ReceiverPostCode = $Draft['Receiver-post-code'];
            $draft_object->ReceiverAddressDetails = $Draft['Receiver-address-detail'];

            $draft_object->Unit = $Draft['unit-weight'];
            $draft_object->Weight = $Draft['weight'];
            $draft_object->Volume = $Draft['volume'];
            $draft_object->PreferredTime = $Draft['preferred-time'];
            $draft_object->PackageDetail = $Draft['package-detail'];
            $draft_object->Fragile = $Draft['fragile-label'];

            $draft_object->TotalCost = $Draft['final-service-cost-hidden'];

            $draft_object->TotalCostString = $draft_object->TotalCost . '';
            $arr_total = array();
            for ($i=strlen($draft_object->TotalCostString)-1; $i >= 0 ; $i--) {
                if (fmod($i, 3) == 1 && $i != strlen($draft_object->TotalCostString)-1) {
                    array_push($arr_total, ',');
                }
                array_push($arr_total, $draft_object->TotalCostString[$i]);
            }
            $draft_object->TotalCostString = '';
            $count = count($arr_total);
            for ($i=0; $i < $count; $i++) {
                $draft_object->TotalCostString .= array_pop($arr_total);
            }

            // var_dump($draft_object);
            // die();
        }
        $Error =  $this->getRequest()->getSession()->get('Error');
        if ($Error != null) {
            $this->getRequest()->getSession()->clear('Error');
        }
        $kota = Kota::get();
        $kecamatan = Kecamatan::get();
        $kodepos = KodePos::get();

        $arr_lokasi = ArrayList::create();
        foreach ($kecamatan as $key => $value) {
            $single_object = DataObject::create();
            $single_object->DataLokasi = $value->Title . ", " . $value->Kota()->Title;
            $arr_lokasi->push($single_object);
        }

        return ['Lokasi' => $arr_lokasi, 'Error' => $Error, 'Draft' => $draft_object];
    }

    public function getDistrict()
    {
        $id = $this->getRequest()->param('ID');
        $district = Kecamatan::get()->filter('KotaID', $id);
        $arr_district = array();
        foreach ($district as $key) {
            $arr = array();
            $arr['ID'] = $key->ID;
            $arr['Title'] = $key->Title;
            $arr_district[] = $arr;
        }
        return json_encode([
            'Status' => 200,
            'Message' => 'Ok',
            'Data' => $arr_district
        ]);
    }

    public function getPostCode()
    {
        $id = $this->getRequest()->param('ID');
        $postcode = KodePos::get()->filter('KecamatanID', $id);
        $arr_postcode = array();
        foreach ($postcode as $key) {
            $arr = array();
            $arr['ID'] = $key->ID;
            $arr['Kode'] = $key->Kode;
            $arr_postcode[] = $arr;
        }
        return json_encode([
            'Status' => 200,
            'Message' => 'Ok',
            'Data' => $arr_postcode
        ]);
    }

    public function setHarga()
    {
        $from = $_GET['from'];
        $to = $_GET['to'];

        $from = explode(',', str_replace(' ', '', $from));
        $from = $from[0];
        $to = explode(',', str_replace(' ', '', $to));
        $to = $to[0];

        $from = Kecamatan::get()->filter('Title', $from)->first();
        $to = Kecamatan::get()->filter('Title', $to)->first();
        $ongkir = HargaOngkir::get()->filter([
            'DariKotaID' => $from->KotaID,
            'DariKotaBagianID' => $from->KotaBagianID,
            'KeKotaID' => $to->KotaID,
            'KeKotaBagianID' => $to->KotaBagianID
        ])->first();

        return json_encode([
            'Status' => 200,
            'Message' => 'OK',
            'Ongkir' => $ongkir->Harga
        ]);
    }

    public function Order()
    {
        // var_dump($_POST);
        // die();
        date_default_timezone_set("Asia/Jakarta");
        $member = Member::currentUser();
        $page = OrderPage::get()->first();
        $session = $this->getRequest()->getSession();

        // Sender Data
        $sendername = isset($_POST['sender-Name']) ? $_POST['sender-Name'] : null;
        $senderemail = isset($_POST['sender-Email']) ? $_POST['sender-Email'] : null;
        $senderphone = isset($_POST['sender-Phone']) ? $_POST['sender-Phone'] : null;
        $senderdistrict = isset($_POST['sender-District']) ? $_POST['sender-District'] : null;
        $senderpostcode = isset($_POST['sender-postcode']) ? $_POST['sender-postcode'] : null;
        $senderaddressdetail = isset($_POST['sender-address-details']) ? $_POST['sender-address-details'] : null;


        if (!$sendername && !$senderemail && !$senderphone && !$senderaddressdetail) {
            $session->set('Error', 'Mohon isi data pengirim');
            return $this->RedirectBack();
        }

        if ($_POST['others-label'] == 'No') {
            $sendercityid = Kota::get()->filter('Title', $sendercityid)->first()->ID;
            $senderdistrictid = Kecamatan::get()->filter('Title', $senderdistrictid)->first()->ID;
        }

        // Receiver Data
        $receivername = isset($_POST['Receiver-Name']) ? $_POST['Receiver-Name'] : null;
        $receiveremail = isset($_POST['Receiver-Email']) ? $_POST['Receiver-Email'] : null;
        $receiverphone = isset($_POST['Receiver-Phone']) ? $_POST['Receiver-Phone'] : null;
        $receiverdistrict = isset($_POST['Receiver-District']) ? $_POST['Receiver-District'] : null;
        $receiverpostcode = isset($_POST['Receiver-post-code']) ? $_POST['Receiver-post-code'] : null;
        $receiveraddressdetails = isset($_POST['Receiver-address-detail']) ? $_POST['Receiver-address-detail'] : null;

        if (!$receivername && !$receiveremail && !$receiverphone && !$receiverdistrict && !$receiverpostcode && !$receiveraddressdetails) {
            $session->set('Error', 'Mohon isi data penerima');
            return $this->RedirectBack();
        }

        // Item Data
        $unit = $_POST['unit-weight'];
        $weight = isset($_POST['weight']) ? $_POST['weight'] : null;
        $volume = isset($_POST['volume']) ? $_POST['volume'] : null;
        $preferredtime = $_POST['preferred-time'] != '' ? $_POST['preferred-time'] : date("h:i:s");
        $packagedetail = isset($_POST['package-detail']) ? $_POST['package-detail'] : null;
        $fragile = $_POST['fragile-label'];

        if ($unit == 'Kilograms') {
            $weight = $weight * 1000;
        }

        if (!$packagedetail) {
            $session->set('Error', 'Mohon isi data barang bertanda "*"');
            return $this->RedirectBack();
        }

        // Order Data
        $cost = $_POST['final-service-cost-hidden'];

        if ($member) {
            $senderdistrict = explode(',', str_replace(' ', '', $senderdistrict));
            $senderdistrictid = Kecamatan::get()->filter('Title', $senderdistrict[0])->first()->ID;
            $sendercityid = Kota::get()->filter('Title', $senderdistrict[1])->first()->ID;

            $receiverdistrict = explode(',', str_replace(' ', '', $receiverdistrict));
            $receiverdistrictid = Kecamatan::get()->filter('Title', $receiverdistrict[0])->first()->ID;
            $receivercityid = Kota::get()->filter('Title', $receiverdistrict[1])->first()->ID;

            $order = Order::create();
            // Sender Data
            $order->PengirimNama = $sendername;
            $order->PengirimEmail = $senderemail;
            $order->PengirimNoTelp = $senderphone;
            $order->PengirimKotaID = $sendercityid;
            $order->PengirimKecamatanID = $senderdistrictid;
            $order->PengirimKodePos = $senderpostcode;
            $order->PengirimAlamat = $senderaddressdetail;
            // Receiver Data
            $order->PenerimaNama = $receivername;
            $order->PenerimaEmail = $receiveremail;
            $order->PenerimaNoTelp = $receiverphone;
            $order->PenerimaKotaID = $receivercityid;
            $order->PenerimaKecamatanID = $receiverdistrictid;
            $order->PenerimaKodePos = $receiverpostcode;
            $order->PenerimaAlamat = $receiveraddressdetails;
            // Item Data
            $order->BarangJenis = $packagedetail;
            $order->BarangBerat = $weight;
            $order->BarangVolume = $volume;
            $order->JamPengambilan = $preferredtime;
            $order->TotalHarga = $cost;
            $order->CustomerID = $member->ID;
            $order->IsFragile = $fragile;

            // var_dump($order);die();
            $order->write();
            return $this->redirect($page->Link().'OrderConfirmation/'.$order->NomorResi);
        }
        $session->set('Draft', $_POST);

        return $this->redirect(CustomerPage::get()->first()->Link() . 'Login?redirect=order');
    }

    public function OrderConfirmation()
    {
        $member = Member::currentUser();
        if (!$member) {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Login');
        }

        $resi = $this->getRequest()->param('ID');
        $order = Order::get()->filter('NomorResi', $resi)->first();
        if ($order->CustomerID != $member->ID) {
            return $this->HttpError('404');
        }

        if ($order->StatusPembayaran == 2) {
            return $this->redirect(CustomerPage::get()->first()->Link() . 'Dashboard');
        }

        return ['Order' => $order];
    }

    public function Track()
    {
        $resi = $this->getRequest()->param('ID');
        $order = Order::get()->filter('NomorResi', $resi)->first();
        if (!$order) {
            return $this->HttpError('404');
        }

        $currentTime = strtotime('now');

        if ($order->Type == 1) {
            return "Sameday deliveries cannot be tracked";
        }

        return $this->customise([
            'Order' => $order,
            'CurrentTime' => $currentTime
        ])->renderWith('TrackPage');
    }

    public function GetOrderStatusAjax()
    {
        $resi = $this->getRequest()->param('ID');
        $order = Order::get()->filter('NomorResi', $resi)->first();
        if (!$order) {
            return json_encode([
                'Status' => 404,
                'Message' => 'Order not found'
            ]);
        }

        $track = OrderTrack::get()->filter([
            'OrderID' => $order->ID
        ]);

        $arrTrack = [];
        $count = 0;
        foreach ($track as $key => $value) {
            $temp = [];
            $temp['Count'] = $count;
            $count++;
            $temp['Timestamp'] = date('d M H:i', strtotime($value->Timestamp));
            $temp['Done'] = true;
            $temp['Desc'] = $value->Title;
            $arrTrack[] = $temp;
        }

        if (count($arrTrack) == 0) {
            $temp = [];
            $temp['Count'] = 0;
            $temp['Timestamp'] = date('d M H:i');
            $temp['Done'] = false;
            $temp['Desc'] = 'Payment accepted';
            $arrTrack[] = $temp;
        }

        if (count($arrTrack) == 1) {
            $temp = [];
            $temp['Count'] = 1;
            $temp['Timestamp'] = date('d M H:i');
            $temp['Done'] = false;
            $temp['Desc'] = 'Courier has received your package';
            $arrTrack[] = $temp;
        }

        if (count($arrTrack) == 2) {
            $temp = [];
            $temp['Count'] = 2;
            $temp['Timestamp'] = date('d M H:i');
            $temp['Done'] = false;
            $temp['Desc'] = 'Your package has been delivered';
            $arrTrack[] = $temp;
        }

        return json_encode([
            'Status' => 200,
            'Message' => 'Sukses get order status',
            'StatusPengiriman' => $order->StatusPengiriman,
            'TrackHistory' => $arrTrack
        ]);
    }
}
