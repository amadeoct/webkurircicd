<?php

use SilverStripe\Admin\ModelAdmin;


/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class LokasiAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'Kota',
        'Kecamatan',
        'HargaOngkir'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'lokasi-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Lokasi';

    private static $menu_icon_class = 'font-icon-safari';
}
