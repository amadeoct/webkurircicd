<?php

use SilverStripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Security\Member;
use SilverStripe\Admin\ModelAdmin;


class Kurir extends Member
{
    private static $db = [
        'NoTelp' => 'Varchar(255)',
        'isGPS' => 'Int',
        'Wallet' => 'Int'
    ];

    private static $has_one = [
        'Image' => Image::class
    ];

    public function onAfterWrite()
    {
        parent::onAfterWrite();
        if ($this->Image()->exists() && !$this->Image()->isPublished()) {
            $this->Image()->doPublish();
        }
    }

    /**
     * CMS Fields
     * @return FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName([
            'Image',
            'DirectGroups',
            'Locale',
            'Permissions',
            'FailedLoginCount',
            'isGPS'
        ]);

        $upload = UploadField::create(
            'Image',
            'Image'
        );
        $upload->setAllowedExtensions([
            'jpeg',
            'jpg',
            'png'
        ]);

        $fields->addFieldToTab(
            'Root.Main',
            $upload,
            'FirstName'
        );

        $fields->addFieldToTab(
            'Root.Main',
            CheckboxField::create(
                'isGPS',
                'Kurir GPS ON'
            ),
            'FirstName'
        );

        return $fields;
    }

    public function toArray()
    {
        $arr = [];
        $arr['ID'] = $this->ID;
        $arr['FullName'] = $this->FirstName . ' ' . $this->Surname;
        $arr['Email'] = $this->Email;
        $arr['Phone'] = $this->NoTelp;
        $arr['ImageURL'] = '';
        if ($this->Image()->exists()) {
            $arr['ImageURL'] = $this->Image()->AbsoluteURL;
        }
        $arr['Duty'] = $this->isGPS;
        $arr['Wallet'] = $this->Wallet;
        return $arr;
    }
}

/**
 * Description
 *
 * @package silverstripe
 * @subpackage mysite
 */
class KurirAdmin extends ModelAdmin
{
    /**
     * Managed data objects for CMS
     * @var array
     */
    private static $managed_models = [
        'Kurir'
    ];

    /**
     * URL Path for CMS
     * @var string
     */
    private static $url_segment = 'kurir-admin';

    /**
     * Menu title for Left and Main CMS
     * @var string
     */
    private static $menu_title = 'Kurir';

    private static $menu_icon_class = 'font-icon-torso';
}
