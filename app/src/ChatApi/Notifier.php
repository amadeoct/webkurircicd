<?php



class Notifier {

    // bosbas real
    // public static $key = "AAAAfVSU_uY:APA91bHKnrGSZMUWUGVcv8OUxH87PPKLRg7C3AzKnT2US0qUH02JmJEg6uqI-AtCeM4wlZRvxC0fhJjpCKudlmc7tkRYd1O_zlJadLhnA7Zp1Eo7Pbl_N87yfkvhTncxs38TdcJITNOx";

    // public static $key = "AAAAjYTjbs0:APA91bERN-DFjesAchABoAVGTx2AFOGA-qOWGoxj2HtgQW8ZyZt0Qbo4H2AXuoOI5p31uMuYCDgjx2p22XoKaXvNTp-BNV_r0vzDrBVrMWJUSSQFIjXXHsUbUo2w17Ux9NRHon34Ktbg";
    // AAAAfVSU_uY:APA91bHKnrGSZMUWUGVcv8OUxH87PPKLRg7C3AzKnT2US0qUH02JmJEg6uqI-AtCeM4wlZRvxC0fhJjpCKudlmc7tkRYd1O_zlJadLhnA7Zp1Eo7Pbl_N87yfkvhTncxs38TdcJITNOx

    public static $key = "AAAAOd8SWSE:APA91bEWdlx4vA5A7PlSWwlxj8n0GN7V3IOVjtuQTHomSFZuIDsVcqKyIQidVEDCcZmjPgV3DeyiPNuAjJc8i0C7qDr65lz3SfBDLpI6GtB6aLzADLS6qa2fC6tEEjzjQBmE2chQrypQ";

    // pinjem xmarks
    // public static $key = 'AAAAFi6vlBs:APA91bEpQFLq9jJVDQ3Z3qDV0lal5XpG0j3Gy8o7hy9yGPGDNxi1ee4Os4uWZFQNy07R9R1L_HAtZxDC884jP2kgArk9lX599p2GEiEuNATWSOOJi_SGkETJcz0jpM0N1W2UYRPOtAJL';

    static function curlPostJson($url, $params, $post, $headers = array()) {
        if (is_string($post)) {
            $post = $params . $post;
        } else {
            $post = $params + $post;
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  // penting, karena SSL
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);      // penting, karena SSL
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if (sizeof($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($curl);
        // Debug::show($response);
        curl_close($curl);
        return $response;

    }



    static function Notify($memberTarget, $data, $arrOutside = []){

        $notif = [];
        $notif['title'] = $data['Title'];
        $notif['body'] = $data['Content'];

        if (!empty($arrOutside)){
            foreach ($arrOutside as $k=>$ao){
                $notif[$k] = $ao;
            }
        }

        // $notif["click_action"] = "FLUTTER_NOTIFICATION_CLICK";
        $data["click_action"] = "FLUTTER_NOTIFICATION_CLICK";
        $post = array(
            'registration_ids' => $memberTarget,
            // 'notification' => $notif,
            'data' => $data,
            // "click_action" => "FLUTTER_NOTIFICATION_CLICK",
            'priority' => 'high'
        );

        $post = json_encode($post);
        $header = array(
            'Authorization:key=' . self::$key ,
            'Content-Type: application/json'
        );

        $result = Notifier::curlPostJson('https://fcm.googleapis.com/fcm/send', $post, (string) '', $header);
        return $result;

    }

}
